import React from "react";
import "./styles/main.css";


import {HashRouter as Router, Switch, Route, Link, withRouter} from "react-router-dom";

import {Home} from "./views/Home/index";
import {motion} from 'framer-motion'
import {Demo} from "./views/Demo/index";
import {Setup} from "./views/Setup/index";
import {Join} from "./views/Join/index";

import firebase from "./services/firebase/index.js";
import {collect} from 'react-recollect';
import {loadWorkshopFromID, updateWorkshop, loadDefault} from './store/updaters/loadWorkshops'

import {store} from 'react-recollect'
import {ColorWrap} from './styles/ColorWrap'
import {WhatIf} from "./views/Whatif";
import {Insights} from "./views/Insights";
import {Inventions} from "./views/Inventions";
import {Sidebar} from './components/Sidebar'
import LottieAnim from './components/LottieAnim'

import {isTouchSupported} from './hooks/Touch'
const variants = {
    loading: {
        opacity: 0.5
    },
    ready: {

        opacity: 1
    }
}

const variantsGrid = {
    visible: i => ({
      opacity: 1,
      transition: {
        delay: i * 0.3,
      },
    }),
    hidden: { opacity: 0 },
  }
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "fxqu",
            defaultID: "fxqu",
            stage: 0,
            alertMessage: "",
            alertState: false,
            loading: true, 
            shouldRefreshData: false
        };


        window.addEventListener('firebaseChanged', () => {
            console.log('firebase state changed');
            // this.checkLocation()
            if(window.location.href.includes("insights")) {
                this.loadData()
                var evt = new Event('insightsUpdated');

                window.dispatchEvent(evt);
            }
        });

        this.ref = firebase
            .firestore()
            .collection("workshops")

    }

    onCollectionUpdate(querySnapshot) {
        console.log("update")


        var evt = new Event('firebaseChanged');
        updateWorkshop().then(()=>{
            window.dispatchEvent(evt);
            console.log(querySnapshot)
        })
    


        // alert("backend updated")
    };

    componentWillMount() {

        // Listener for firebase changes
        this.unsubscribe = this
            .ref
            .onSnapshot(this.onCollectionUpdate);

        this.unlisten = this
            .props
            .history
            .listen((location, action) => {
                console.log(location);
                this.checkLocation(location)
            });

    }
    componentWillUnmount() {
        this.unlisten();
    }

    loadData() {
        if(localStorage.getItem("session")) {
            let session = JSON.parse(localStorage.getItem("session"))
            // console.log(session)
            console.log("Current Session", session.id)
            let now = new Date()
            let then = new Date(session.time)
            if(now.getTime() - then.getTime()> 86400000 ) {
                // Load Default
                loadDefault(this.ref, this.state.id).then(() => {
                    this.setState({loading: false})
                    // this.setState()
                }).catch(() => {
                    this.setState({loading: true})
        
                })
            } else {
                loadDefault(this.ref, session.id).then(() => {
                    this.setState({loading: false})
                    // this.setState()
                }).catch(() => {
                    // this.setState({loading: true})
                    this.props.history.push("/")
        
                })
            }
          

        } else {
            loadDefault(this.ref, this.state.id).then(() => {
                this.setState({loading: false})
                // this.setState()
            }).catch(() => {
                this.setState({loading: true})
    
            })
        }
    }

    componentDidMount() {
        console.log("mounted", this.props.location.pathname)
        this.checkLocation(this.props.location)

        // To check if there is something loaded already
        this.loadData()





     

        let queryString = window.location.search;
        queryString = queryString
            .slice(1, 5)
            .toString()
        console.log(queryString.length)

        if (queryString && queryString.length == 4) {
            loadDefault(this.ref, queryString).then(() => {
                localStorage.setItem("session", JSON.stringify({time: new Date(), id: queryString }))

                this
                    .props
                    .history
                    .push("/whatif")

            }).catch(() => {
                console.log("workshop not found")
                this.setState({alertMessage: "workshop not found, please try another code", alertState: true})
                this
                    .props
                    .history
                    .push("/join")

            })
        }


        this.checkIfAndroid()
        store.touch = isTouchSupported()
        
    }

    checkIfAndroid() {

        //This is to fix a weird height bug on Android
        let ua = window.navigator.userAgent
        let height = window.innerHeight
        var isAndroid = /(android)/i.test(navigator.userAgent);

            if(isAndroid) {
                setInterval(()=>{
                    if(document.querySelector(".cards-flex-container")) {
                        document.querySelector(".cards-flex-container").setAttribute("style",`height:${height}px`);

                    }
                },500)
                
            
            }

    }

    checkLocation(location) {
        console.log(location.pathname)
        switch (location.pathname) {
            case "/whatif":
                this.setState({stage: 1})
                this.setState({shouldRefreshData: false})

                // this.controls.set
                break;
            case "/inventions":
                this.setState({stage: 2})
                this.setState({shouldRefreshData: false})

                break;
            case "/insights":
                this.setState({shouldRefreshData: true})

                this.setState({stage: 3})
                break;
            case "/demo":
                this.setState({stage: 1})
                this.setState({shouldRefreshData: false})

                break;
            case "/":
                this.setState({stage: 0})
                this.setState({shouldRefreshData: false})

                break;
            default:
                this.setState({stage: 0})
                this.setState({shouldRefreshData: false})

                break;
        }
    }
    render() {

        if (!this.state.loading) {

            return (
                <ColorWrap stage={this.state.stage}>
                    <Switch>
                        <Route path="/demo">
                            <Demo history={this.props.history}></Demo>
                        </Route>
                        <Route path="/setup">
                            <Setup history={this.props.history}></Setup>
                        </Route>
                        <Route path="/join">
                            <Join
                                history={this.props.history}
                                alert={{
                                state: this.state.alertState,
                                message: this.state.alertMessage
                            }}></Join>
                        </Route>
                        <Route path="/whatif">
                            <WhatIf></WhatIf>
                            <Sidebar></Sidebar>
                        </Route>
                        <Route path="/inventions">
                            <Inventions></Inventions>
                            <Sidebar></Sidebar>

                        </Route>
                        <Route path="/insights">
                            <Insights></Insights>
                            <Sidebar></Sidebar>

                        </Route>
                        <Route path="/">
                            <motion.div
                                animate={this.state.isLoading
                                ? "loading"
                                : "ready"}
                                variants={variants}
                                className="flex-container">

                                <Home></Home>
                            </motion.div>
                        </Route>
                    </Switch>
                </ColorWrap>

            );
        } else {
            return (
                <div className="flex-container">
                   <div className="lottie-anim">
                       <h1 style={{color:"white", textAlign:"center"}}>Loading...</h1>
                                        <LottieAnim width={"34%"} height={"30%"} animation={"create-cards"}></LottieAnim>
                    </div>
                </div>
            )
        }
    }
}

export default withRouter(collect(App))
