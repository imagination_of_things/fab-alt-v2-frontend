import * as React from "react";
import { Stack, Frame } from "framer";
import {motion} from 'framer-motion'
import { Icon } from "./Icons";
import { Link } from "react-router-dom";
import { createBrowserHistory } from "history";
import '../styles/main.css'
// const history = createBrowserHistory();

const defaultProps = {
  showLabels: true,
  labels: [0, 1, 2],
  routes: ["/whatif", "/inventions", "/insights"],
  width: 500,
  height: "11%"
};
const stackLayout = {
  width: "100%",
  height: "100%",
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  alignContent: "flex-end",
  overflow: "hidden",
  backgroundColor: "transparent"
};

const buttonmotion = {
  display: "flex",
  flexDirection:"column",
  alignContent: "center",
  justifyContent: "center",
  alignItems:"center",
  zIndex: 3,
  
  // padding: "0px 40px",
  
  
  // height: "100%",
  
  // backgroundColor: "transparent"
};

const textVariants= {
  visible: { opacity: 1, y: 0, scale:1 },
  hidden: { opacity: 0, y: -100, scale:0 },
}

export const BottomNavigationBar = (props) => {
  const { labels, routes, page, ...other } = props;
  // tslint:disable-next-line: ban-ts-ignore
  // @ts-ignore
  const [value, setValue] = React.useState(0);

  const items = defaultProps.labels;
  const handleChange = (event, newValue) => {
    setValue(newValue);
    // props.handleChange(newValue);
    props.shareToggle(false)
  };
  return (
    <div className="bottom-nav-bar">
      <motion.div
        style={buttonmotion}
        whileTap={{ scale: 0.8 }}
        onClick={() => {
          handleChange({}, 1);
        }}
      >
        <Link to="/whatif">
          <Icon icon={0} />
        </Link>
        <motion.div variants={textVariants} initial="hidden" animate={page===0?"visible":"hidden"} className="navigation-text" >
          Concept
        </motion.div>
      </motion.div>
      <motion.div
        style={buttonmotion}
        whileTap={{ scale: 0.8 }}
        onClick={() => {
          handleChange({}, 2);
        }}
      >
        <Link to="/inventions">
          <Icon icon={1} />
        </Link>
        <motion.div variants={textVariants} initial="hidden" animate={page===1?"visible":"hidden"} className="navigation-text" >
          Inventions
        </motion.div>
     
      </motion.div>
      <motion.div
        style={buttonmotion}
        whileTap={{ scale: 0.8 }}
        onClick={() => {
          handleChange({}, 3);
        }}
      >
        <Link to="/insights">
          <Icon icon={2} />
        </Link>
        <motion.div variants={textVariants} initial="hidden" animate={page===2?"visible":"hidden"} className="navigation-text" >
          Insight
        </motion.div>
      </motion.div>
    </div>
  );
};


