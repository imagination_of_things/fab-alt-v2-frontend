import * as React from "react";
import {
    Frame,
    Stack,
    Page,
    transform,
    Color,
    addPropertyControls,
    ControlType
} from "framer";
import * as RiTa from "rita/lib/rita-tiny";
import {Icon} from "./Icons";
import {motion} from "framer-motion"
import "../styles/CardStyles.css"
import { TweenLite, Power4,  CSSPlugin} from 'gsap'
import BackupData from '../store/BackupData'
import HyperButton from './HyperButton'
import { store } from "react-recollect";
import {addInsight} from "../store/updaters/addInsights"
const tagCategories = [
    "jj",
    "nn",
    "nns",
    "vbd",
    "vbz",
    "vbp",
    "vbg",
    'vbn',
    'rb',
    "vb"
];
const nonActive = ["were", "was", "Is"]
const plugins = [CSSPlugin];



function LightBulb(props) {

    if (props.insightsExist) {
        return (
            <div>
              
          
             
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="30">
                <path
                    d="M 8.125 4.087 C 12.612 4.087 16.25 7.724 16.25 12.212 C 16.25 14.542 15.269 16.643 13.697 18.125 L 13.75 18.125 L 10 23.125 L 6.25 23.125 L 2.5 18.125 L 2.553 18.125 C 0.981 16.643 0 14.542 0 12.212 C 0 7.724 3.638 4.087 8.125 4.087 Z"
                    fill="hsl(0, 0%, 100%)"></path>
                <path
                    d="M 5 24.087 C 5 23.396 5.56 22.837 6.25 22.837 L 10 22.837 C 10.69 22.837 11.25 23.396 11.25 24.087 L 11.25 26.587 C 11.25 27.277 10.69 27.837 10 27.837 C 10 28.527 9.44 29.087 8.75 29.087 L 7.5 29.087 C 6.81 29.087 6.25 28.527 6.25 27.837 C 5.56 27.837 5 27.277 5 26.587 Z"
                    fill="hsl(0, 0%, 20%)"></path>
                <path
                    d="M 13.125 0.337 L 19.375 0.337 L 15.625 6.587 L 20 5.962 L 9.375 15.962 L 13.125 9.087 L 8.75 9.712 Z"
                    fill="rgba(223, 223, 119, 1.00)"></path>
            </svg>
            </div>

        )
    } else {

        return (
            <div>
              
                {/* <svg id={props.id+"outer"}   xmlns="http://www.w3.org/2000/svg" className="insight-button-anim" height="20" width="20" viewBox="-5 -5 20 20">
                    <g transform="translate(22274.234,4568.4141)">
                    <path d="m-22257-4557.6c0,4.4183-3.582,8-8,8s-8-3.5817-8-8,3.582-8,8-8,8,3.5817,8,8z" transform="matrix(0.87469499,0.01727284,-0.01727284,0.87469499,-2869.8629,-189.29209)" stroke="#ffff88" strokeDasharray="none" strokeMiterlimit="4" strokeWidth="3.28846145" fill="none"/>
                    </g>
                </svg> */}
        
            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="26">
                <path
                    d="M 8.125 0.087 C 12.612 0.087 16.25 3.724 16.25 8.212 C 16.25 10.542 15.269 12.643 13.697 14.125 L 13.75 14.125 L 10 19.125 L 6.25 19.125 L 2.5 14.125 L 2.553 14.125 C 0.981 12.643 0 10.542 0 8.212 C 0 3.724 3.638 0.087 8.125 0.087 Z"
                    fill="hsl(0, 0%, 100%)"></path>
                <path
                    d="M 5 20.087 C 5 19.396 5.56 18.837 6.25 18.837 L 10 18.837 C 10.69 18.837 11.25 19.396 11.25 20.087 L 11.25 22.587 C 11.25 23.277 10.69 23.837 10 23.837 C 10 24.527 9.44 25.087 8.75 25.087 L 7.5 25.087 C 6.81 25.087 6.25 24.527 6.25 23.837 C 5.56 23.837 5 23.277 5 22.587 Z"
                    fill="hsl(0, 0%, 20%)"></path>
            </svg>
            </div>
        )
    }
}
function getRandomString(string_length=8){
    let random_string = '';
    let random_ascii;
    for(let i = 0; i < string_length; i++) {
        random_ascii = Math.floor((Math.random() * 25) + 97);
        random_string += String.fromCharCode(random_ascii)
    }
    return random_string
}

export class Card extends React.Component {
    constructor(props) {
        super(props);
        this.nextAdjective = false;
        this.state = {
            wordInfo: [],
            isSaving: false, 
            isInsightAdding: true, 
            id: ''
        
        }


    }
    isVowel(char) {
        return /^[aeiou]$/.test(char.toLowerCase());
    }

    componentWillMount() {

        this.setState({id: getRandomString()})


        let words = RiTa.tokenize(this.props.title);
        let tags = RiTa.getPosTags(this.props.title.toLowerCase());
        let wordInfo = []
        tags.forEach((tag, index) => {
            let active = false;
            if (tagCategories.includes(tag) && !nonActive.includes(words[index])) {
                active = true;
            }
            // ////console.log(tag);

            if (words[index] === "a" || words[index] === "an") {
                this.nextAdjective = true;
            } else {

                if (this.isVowel(words[index][0]) && this.nextAdjective) {
                    this.nextAdjective = false
                    wordInfo.push({
                        word: "an " + words[index],
                        tag: tag,
                        active: active
                    });

                } else if (!this.isVowel(words[index][0]) && this.nextAdjective) {
                    this.nextAdjective = false
                    wordInfo.push({
                        word: "a " + words[index],
                        tag: tag,
                        active: active
                    });

                } else {
                    wordInfo.push({word: words[index], tag: tag, active: active});

                }

            }
        });
        this.setState({wordInfo: wordInfo})
        //console.log(wordInfo)
    }

    getRandomSubarray(arr, size) {
        var shuffled = arr.slice(0),
            i = arr.length,
            temp,
            index;
        while (i--) {
            index = Math.floor((i + 1) * Math.random());
            temp = shuffled[index];
            shuffled[index] = shuffled[i];
            shuffled[i] = temp;
        }
        return shuffled.slice(0, size);
    }

    changeWord(index) {
        let tag = this.state.wordInfo[index].tag
        let words = []
        // Special case for inventions
        if(this.state.wordInfo[0].word === "Is") {
             words = this.props.words[tag.toUpperCase()]

            if(index === 4) {
                words = this.props.words["nn"] + this.props.words["nns"]
            }

        } else {
             words = this.props.words[tag.toUpperCase()]

        }
        //console.log(tag)
        if (!words) {
            words = BackupData[tag]
        }
        // Flip a coin to infuse our database
        if (words.length < 5) {
            words = [
                ...this.getRandomSubarray(BackupData[tag], 10),
                ...words
            ]
        }
       
        let newWord = words[Math.floor(Math.random() * words.length - 1)]
        // Flip coin to infuse magic
        if (Math.random() < 0.2) {

            if (BackupData[tag]) {
                newWord = BackupData[tag][Math.floor(Math.random() * BackupData[tag].length - 1)]
                //console.log("magic")
            }

        }

        //Make sure word exists
        if (newWord && newWord.length > 1) {

            if (this.state.wordInfo[4].word === "was" && this.state.wordInfo[5].tag != 'vbg') {
                if (index === 2 || index === 5) {
                    if (this.isVowel(newWord[0])) {
                        newWord = "an " + newWord
                    } else {
                        newWord = "a " + newWord
                    }
                }
            } else if (this.state.wordInfo[4].word === "was" && this.state.wordInfo[5].tag === 'vbg') {
                if (index === 2 || index === 6) {
                    if (this.isVowel(newWord[0])) {
                        newWord = "an " + newWord
                    } else {
                        newWord = "a " + newWord
                    }
                }
            } else if(this.state.wordInfo[0].word === "Is") {
                if (index === 2) {
                    if (this.isVowel(newWord[0])) {
                        newWord = "an " + newWord
                    } else {
                        newWord = "a " + newWord
                    }
                }
            }
            let newState = this.state.wordInfo

            //console.log(newWord)
            newState[index] = {
                word: newWord,
                tag: tag,
                active: true
            }

            this.setState({wordInfo: newState})

        }

        // if(this.isVowel(words[index][0]) && this.nextAdjective) { this.nextAdjective
        // = false     this.wordInfo.push({word: "an " + words[index], tag: tag, active:
        // active}); } else if(!this.isVowel(words[index][0]) && this.nextAdjective) {
        // this.nextAdjective = false     this.wordInfo.push({word: "a " + words[index],
        // tag: tag, active: active}); } else {     this.wordInfo.push({word:
        // words[index], tag: tag, active: active}); }

    }

    updateWord(word, index) {
        // //console.log(event.target.value, index)

        if (word && word.length > 0) {
            let newState = this.state.wordInfo
            let tags = RiTa.getPosTags(word)

            newState[index] = {
                word: word,
                tag: tags[0],
                active: true
            }

            this.setState({wordInfo: newState})
            //console.log(newState[index])
        }
    }
    collectSentence() {


        let sentence = ""
        


        this.state.wordInfo.forEach((word, index)=>{
            //console.log(word)  

            if(word.word != "?" && index<this.state.wordInfo.length-2) {
                sentence += word.word + " ";

            } else {
                sentence += word.word;
            }


        })

        //console.log(sentence)
        addInsight(sentence)


    }

    animation() {
        let elem = document.querySelector('#'+this.state.id+"outer")
        TweenLite.to(elem, 0.35, {
            scale: 25, 
            opacity: 0.75,
            
            ease: "power4.inOut",
            onComplete: ()=>{
                TweenLite.to(elem, 0.15, {
                    delay: 0.1,
                    opacity: 0,

                    onComplete: ()=>{
                        TweenLite.to(elem, 0.15, {
                            scale: 1,

                        })
                    }

            
                })
            }
        })
    }

    render() {
        return (
            <motion.div
                className="card-container"
               
               >
                <motion.div className="card-inner-container">

                    {this
                        .state
                        .wordInfo
                        .map((data, index) => {
                            return <HyperButton
                                text={data.word}
                                active={data.active}
                                tag={data.tag}
                                key={index}
                                words={this.props.words}
                                updateWord={(word) => {
                                this.updateWord(word, index)
                            }}
                                changeHandler={() => {
                                if (data.active) {
                                    this.changeWord(index)
                                }
                            }}/>
                        })}

                </motion.div>
                <motion.div className="card-bottom">
                <svg id={this.state.id+"outer"} style={{opacity: 0}} xmlns="http://www.w3.org/2000/svg" className="insight-button-anim" height="20" width="20" ve>
                        <g transform="translate(22274.234,4568.4141)">
                        <path d="m-22257-4557.6c0,4.4183-3.582,8-8,8s-8-3.5817-8-8,3.582-8,8-8,8,3.5817,8,8z" transform="matrix(0.87469499,0.01727284,-0.01727284,0.87469499,-2869.8629,-189.29209)" stroke="#ffff88" strokeDasharray="none" strokeMiterlimit="4" strokeWidth="3.28846145" fill="none"/>
                        </g>
                    </svg>
                    <motion.div id="lightbulb" 
                     whileTap={{opacity:1}}
                     whileHover={{opacity:1}}
                     style={{opacity:0.6}}
                    onClick={() => {
                        if(!store.touch) {
                            this.collectSentence()
                            this.animation()
                        }
                      
                       
                    }}
                    onTouchStart={()=>{
                        if(store.touch) {
                            this.collectSentence()
                            this.animation()
                        }
                    }}
                    >
                        <LightBulb id={this.state.id} insightsExist={store.selectedWorkshop.insights} />

                    </motion.div>
                </motion.div>
            </motion.div>
        );
    }
}
