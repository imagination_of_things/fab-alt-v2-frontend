import * as React from "react";
import { Frame, Page, transform, Color } from "framer";
import { Card } from "./Card";
import {store} from 'react-recollect'
import {withBreakpoints} from 'react-breakpoints'


const pages = ["One", "Two", "Three", "Four"];

const style = {
  fontSize: 32,
  fontWeight: 800,
  width: "90vw",
  color: "#000",
  // height: "100%",
  zIndex: "123123"
};

const pageStyle = {
  borderRadius: 20,
  zIndex: 5,
  position: "relative"

  // height: "98%"
};
const blue = Color("#ffffff75");
const text = Color("#000000");
const textNone = Color("#00000000");

const darkBlue = Color("#00000050");
const shadowCard = "0px 7px 7px 0px rgba(0, 0, 0, 0.25)";
const noShadowCard = "0px 0px 0px 0px rgba(0, 0, 0, 0.25)";


const variants = {
    open: {
       opacity: 1, y: 0 
    }, 
    closed: {
       opacity: 0, y: -1001 
    }
}

class CardDeck extends React.Component {
  constructor(props) {
    super(props);
    this.color = this.props.color ? Color(this.props.color) : blue;
    this.darkenAmount = this.props.dark ? this.props.dark : 40
    this.dark = this.props.color ? Color.darken(this.color, this.darkenAmount) : darkBlue;
    this.state = {
      isOpen: false
    }
  }

  componentDidMount() {
    if(store.selectedWorkshop) {
      this.setState({isOpen: true})

      if(this.props.id === "whatif") {
        this.setState({data: store.selectedWorkshop.whatif.slice(0,15), words: store.selectedWorkshop.words})

      } else if (this.props.id === "invention") {
        this.setState({data: store.selectedWorkshop.inventions.slice(0,15), words: store.selectedWorkshop.words})

      } else {
        // this.setState({data: store.selectedWorkshop.inventions.slice(0,15), words: store.selectedWorkshop.words})
        this.setState({isOpen: false})

      }
      ////console.log("this.ststa", this.state)
      // clearInterval(interval);

    }
  }

  componentWillMount() {
    ////console.log(store)

    let interval = setInterval(()=>{

      ////console.log("bleeop")
    }, 500)

  }

  handleCardSave(index) {

  }
  render() {
    const { breakpoints, currentBreakpoint } = this.props
    if(breakpoints[currentBreakpoint] > breakpoints.tablet) {

    return (
      <div className="cards-content">
      <Page
        width={"82%"}
        alignment="center"
        height={"78%"}

        // scaleX={"0.8"}
        left={"8%"}
        overflow={"visible"}
        style={pageStyle}
        initial={"closed"}
        animate={this.state.isOpen ? "open" : "closed" }
        variants={variants}
        transition={{ delay: 0.1, duration: 0.25 }}
        // gap={-50}
        effect={info => {
          const offset = info.normalizedOffset;

          const background = transform(
            offset,
            [-1, 0, 1],
            [this.dark, this.color, this.dark]
          );
          const color = transform(
            offset,
            [-1, 0, 1],
            [textNone, text, textNone]
          );

          const scaleY = transform(offset, [-1, 0, 1], [0.9, 1, 0.9]);
          // const x = transform(offset, [-1, 0, 1], [50, , -50]);

          const borderRadius = transform(offset, [-1, 0, 1], [20, 20, 20]);
          const boxShadow = transform(
            offset,
            [-1, 0, 1],
            [noShadowCard, shadowCard, noShadowCard]
          );

          return {
            color,
            background,
            borderRadius,
            scaleY,
                      
            boxShadow
          };
        }}
      >
        {this.state.data ? this.state.data.map((title, index) => {
          
          return <Card type={this.props.id} title={title} key={index} words={this.state.words}/>;
        }) : null}
      </Page>
      </div>
    );
      } else {
        // MOBILE DECK
        return (
          <div className="cards-content">

          <Page
            width={"97%"}
            maxWidth={"200px"}
            height={"93%"}
            top={"4%"}
            alignment="center"
            // x={"2.5%"}
            overflow={"visible"}
            style={pageStyle}
            animate={this.state.isOpen ? "open" : "closed" }
            variants={variants}
            transition={{ delay: 0.1, duration: 0.25 }}
            // gap={-50}
            effect={info => {
              const offset = info.normalizedOffset;
    
              const background = transform(
                offset,
                [-1, 0, 1],
                [this.dark, this.color, this.dark]
              );
              const color = transform(
                offset,
                [-1, 0, 1],
                [textNone, text, textNone]
              );
    
              const scaleY = transform(offset, [-1, 0, 1], [0.85, 1, 0.85]);
              const scaleX = transform(offset, [-1, 0, 1], [1.02, 1, 1.02]);
    
              const borderRadius = transform(offset, [-1, 0, 1], [20, 20, 20]);
              const boxShadow = transform(
                offset,
                [-1, 0, 1],
                [noShadowCard, shadowCard, noShadowCard]
              );
    
              return {
                color,
                background,
                borderRadius,
                scaleY,
                scaleX,
                boxShadow
              };
            }}
          >
            {this.state.data ? this.state.data.map((title, index) => {
              
              return <Card type={this.props.id} title={title} key={index} words={this.state.words} deckSavingAnimation={()=>{
                this.handleCardSave(index);
              }}/>;
            }) : null}
          </Page>
          </div>
        )
      }
  }
}
export default withBreakpoints(CardDeck)
