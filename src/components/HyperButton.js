import * as React from 'react'
import {motion} from 'framer-motion'
import "../styles/CardStyles.css"
import LongPress from './LongPress'

import {store} from 'react-recollect'
const buttonVariants = {
    active: {
        backgroundColor: "#000000"

    },
    inActive: {
        backgroundColor: "transparent"

    }
}
export default function HyperButton(props) {
    const [isEditing,
        setEditing] = React.useState(false)

    const [timeoutID, setTID] = React.useState(0)
 
    const [internalWord,
        setInternalWord] = React.useState("")

    function changeWord() {

        props.changeHandler();

    }

    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            console.log('enter press here! ')
            handleSubmit()
        }
    }

    const updateInputValue = (event) => {

        setInternalWord(event.target.value)
        // console.log(internalWord)

    }

    const handleSubmit = (event) => {

        setEditing(false);
        props.updateWord(internalWord.toLowerCase());
    }

    if (isEditing && props.active) {
        return (
            <motion.div className="hyperbutton">

                <input
                    maxLength="14"
                    onChange={evt => {
                    updateInputValue(evt)
                }}
                    onBlur={() => {
                    handleSubmit()
                }}
                    onKeyPress={handleKeyPress}
                    className="hyperbutton-text-input"
                    id={props.text}
                    placeholder={props.text}></input>

            </motion.div>
        )
    } else {
        return (
            <LongPress
                time={500}
                onLongPress={()=>{
                    setEditing(true)
                    document.getElementById(props.text).focus()
                }}
                onPress={()=>{
                    changeWord()
                }}
                >
            <motion.div
                className="hyperbutton"
                variants={buttonVariants}
                animate={props.active
                ? "active"
                : "inActive"}
                onClick={()=>{

                    if(!store.touch){
                    changeWord()
                    }
                }}
                onMouseDown={()=>{
                    let tid = setTimeout(()=>{setEditing(true)
                        document.getElementById(props.text).focus()

                    }, 500)
                    setTID(tid)
                }}
                onMouseUp={()=>{
                    clearTimeout(timeoutID)
                }}
                onMouseLeave={()=>{
                    clearTimeout(timeoutID)
                }}
            >
                      
               <motion.span className="hyperbutton-text">{props.text}</motion.span>

            </motion.div>
            </LongPress>
        )
    }
}