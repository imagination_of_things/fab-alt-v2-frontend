import * as React from "react";
import {
    Frame,
    Stack,
    Page,
    transform,
    Color,
    addPropertyControls,
    ControlType
} from "framer";
import * as RiTa from "rita/lib/rita-tiny";
import {Icon} from "./Icons";
import {motion} from "framer-motion"
import "../styles/CardStyles.css"
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import BackupData from '../store/BackupData'
import HyperButton from './HyperButton'

const tagCategories = [
    "jj",
    "nn",
    "nns",
    "vbd",
    "vbz",
    "vbp",
    "vbg",
    'vbn',
    'rb',
    "vb"
];

const nonActive = ["were", "was", "Is"]

const variants = {
    whatif: {
        backgroundColor: "#768fff"
    },
    invention: {
        backgroundColor: "#ff5131"
    },
    insight: {
        backgroundColor: "#181818"
    }
}

function GraphicWhat() {
    return (
        <svg className="svg-content" viewBox="0 0 147 147" width="100%" height="100%">
            <path
                d="M 52 72.651 L 63.642 72.651 C 63.962 72.651 64.278 72.667 64.59 72.699 C 66.375 68.562 70.491 65.666 75.284 65.666 C 81.713 65.666 86.925 70.878 86.925 77.308 C 86.925 78.105 86.845 78.884 86.692 79.636 L 88.09 79.636 C 91.304 79.636 93.91 82.242 93.91 85.457 C 93.91 88.672 91.304 91.278 88.09 91.278 L 52 91.278 C 46.856 91.278 42.687 87.108 42.687 81.965 C 42.687 76.821 46.856 72.651 52 72.651 Z"
                fill="hsl(229, 100%, 86%)"></path>
            <path
                d="M 74.701 44.821 C 79.952 44.821 84.317 48.612 85.207 53.607 C 85.948 53.444 86.718 53.358 87.507 53.358 C 93.401 53.358 98.179 58.136 98.179 64.03 C 98.179 64.761 98.106 65.475 97.966 66.164 L 99.246 66.164 C 102.193 66.164 104.582 68.553 104.582 71.5 C 104.582 73.246 103.743 74.797 102.446 75.77 L 91.776 86.44 L 80.393 76.836 L 69.366 76.836 C 66.419 76.836 64.03 74.447 64.03 71.5 C 64.03 69.038 65.698 66.965 67.965 66.35 L 57.627 57.627 L 66.161 49.093 C 68.108 46.499 71.209 44.821 74.701 44.821 Z"
                fill="rgba(0, 58, 203, 1.00)"></path>
            <path
                d="M 88.575 76.836 C 91.522 76.836 93.91 79.225 93.91 82.172 C 93.91 85.119 91.522 87.507 88.575 87.507 L 55.493 87.507 C 50.778 87.507 46.955 83.685 46.955 78.97 C 46.955 74.665 50.141 71.105 54.284 70.518 C 53.689 69.188 53.358 67.715 53.358 66.164 C 53.358 60.27 58.136 55.493 64.03 55.493 C 69.28 55.493 73.645 59.284 74.535 64.278 C 74.591 64.266 74.646 64.255 74.701 64.243 C 75.391 64.103 76.105 64.03 76.836 64.03 C 82.73 64.03 87.507 68.808 87.507 74.701 C 87.507 75.432 87.434 76.146 87.294 76.836 Z"
                fill="hsl(0, 0%, 100%)"></path>
        </svg>
    )
}

function GraphicBox() {
    return (
        <svg
            className="svg-content"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 147 147"
            width="100%"
            height="100%">
            <path
                d="M 52.657 63.627 L 83.373 63.627 L 83.373 94.343 L 52.657 94.343 Z"
                fill="hsl(9, 100%, 71%)"></path>
            <path
                d="M 83.373 63.627 L 98.731 50.463 L 98.731 81.179 L 83.373 94.343 Z"
                fill="hsl(9, 71%, 43%)"></path>
            <path
                d="M 52.657 63.627 L 68.015 50.463 L 98.731 50.463 L 83.373 63.627 Z"
                fill="hsl(0, 0%, 100%)"></path>
        </svg>
    )
}

function GraphicMight() {
    return (
        <svg
            className="svg-content"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 153 114"
            width="100%"
            height="100%">
            <path
                d="M 76.5 55.334 C 84.698 55.334 91.343 61.969 91.343 70.154 C 91.343 74.405 89.551 78.238 86.679 80.94 L 86.776 80.94 L 79.925 90.06 L 73.075 90.06 L 66.224 80.94 L 66.321 80.94 C 63.449 78.238 61.657 74.405 61.657 70.154 C 61.657 61.969 68.302 55.334 76.5 55.334 Z"
                fill="hsl(0, 0%, 100%)"></path>
            <path
                d="M 70.791 91.2 C 70.791 89.941 71.812 88.92 73.071 88.92 L 79.929 88.92 C 81.188 88.92 82.209 89.941 82.209 91.2 L 82.209 95.76 C 82.209 97.019 81.188 98.04 79.929 98.04 L 79.925 98.04 C 79.925 99.299 78.905 100.32 77.645 100.32 L 75.355 100.32 C 74.095 100.32 73.075 99.299 73.075 98.04 L 73.071 98.04 C 71.812 98.04 70.791 97.019 70.791 95.76 Z"
                fill="rgba(196, 194, 25, 1.00)"></path>
            <path
                d="M 84.493 49.02 L 95.91 49.02 L 89.06 60.42 L 97.052 59.28 L 77.642 77.52 L 84.493 64.98 L 76.5 66.12 Z"
                fill="rgba(223, 223, 119, 1.00)"></path>
            <path
                d="M 91.343 44.46 L 102.761 44.46 L 95.91 55.86 L 103.903 54.72 L 84.493 72.96 L 91.343 60.42 L 83.351 61.56 Z"
                fill="#FAF455"></path>
        </svg>
    )
}

export class InsightCard extends React.Component {
    constructor(props) {
        super(props);
        this.nextAdjective = false;
        this.state = {
            insights: [],
            insightsIndex: 0,
            sentence: "",
            isSaving: false
        }

    }

    componentDidMount() {
        console.log(this.props)

    }

    render() {

        if (this.props.type === "insights") {

            return (
                <motion.div
                    className="insights-card-container"
                    animate={{
                    backgroundColor: "#181818",
                    color: "fff"
                }}>
                    <div className="insights-top-banner">
                        <div className="insights-bare">
                            <GraphicMight></GraphicMight>

                        </div>
                        <div className="insights-card-title">
                            Machine Insight
                        </div>
                    </div>
                    <motion.div className="machine-text-div">

                        <div className="insights-card-input">{this.props.insights[this.state.insightsIndex]}</div>
                    </motion.div>

                    <motion.div className="insights-bottom-bar">
                        <div
                            className="insights-refresh"
                            onClick={() => {
                            this.setState({
                                insightsIndex: (this.state.insightsIndex + 1) % this.props.insights.length
                            })
                        }}>
                            <IconButton style={{padding:"0px 20px"}}aria-label="delete">
                                <RefreshIcon/>
                            </IconButton>
                        </div>
                    </motion.div>

                </motion.div>
            );
        } else if (this.props.type === "invention") {
            return (
                <motion.div
                    className="insights-card-container"
                    animate={this.props.type === "whatif"
                    ? "whatif"
                    : "invention"}
                    variants={variants}>
                    <div className="insights-top-banner">
                        <div className="insights-bare">
                            <GraphicBox></GraphicBox>

                        </div>
                        <div className="insights-card-title">
                            Invention
                        </div>
                    </div>

                    <motion.div className="insights-text-div">
                        {this.props.sentence}
                    </motion.div>

                </motion.div>
            );
        } else {
            return (
                <motion.div
                    className="card-container"
                    animate={this.props.type === "whatif"
                    ? "whatif"
                    : "invention"}
                    variants={variants}>
                    <div className="insights-top-banner">
                        <div className="insights-bare">
                            <GraphicWhat></GraphicWhat>

                        </div>
                        <div className="insights-card-title">
                            Concept
                        </div>
                    </div>
                    <motion.div className="insights-text-div">
                        {this.props.sentence}
                    </motion.div>

                </motion.div>
            );
        }
    }
}
