import * as React from "react";
import { Frame, Page, transform, Color } from "framer";
import { InsightCard } from "./InsightCard";
import {store} from 'react-recollect'
import {withBreakpoints} from 'react-breakpoints'

import {loadWorkshopFromID} from '../store/updaters/loadWorkshops'

const pages = ["One", "Two", "Three", "Four"];

const style = {
  fontSize: 32,
  fontWeight: 800,
  width: "90vw",
  color: "#000",
  // height: "100%",
  zIndex: "123123"
};

const pageStyle = {
  borderRadius: 20,
  zIndex: 5,
  position: "relative"

  // height: "98%"
};
const blue = Color("#ffffff45");
const text = Color("#000000");
const textNone = Color("#00000000");

const darkBlue = Color("#00000050");
const shadowCard = "0px 7px 7px 0px rgba(0, 0, 0, 0.25)";
const noShadowCard = "0px 0px 0px 0px rgba(0, 0, 0, 0.25)";


const variants = {
    open: {
       opacity: 1, y: 0 
    }, 
    closed: {
       opacity: 0, y: -1001 
    }
}

function AreThereInsights(props) {

      return (
          <div className="insights-no-content">
              <LightBulbGraphic></LightBulbGraphic>
              <div className="insights-text">
                  There aren't any insights yet.
                  <br/>
                  <p className="text-small">Save your cards as insights with the light bulb</p>
              </div>
          </div>
      )
  
}

function LightBulbGraphic() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="107" height="120">
      <path d="M 0 52.684 C 0 23.588 23.947 0 53.487 0 C 83.027 0 106.974 23.588 106.974 52.684 C 106.974 81.781 83.027 105.368 53.487 105.368 C 23.947 105.368 0 81.781 0 52.684 Z" fill="rgb(255,255,255)" opacity="0.17"></path>
      <path d="M 87.091 52.938 C 87.065 33.912 70.328 18.747 50.573 20.931 C 35.486 22.583 23.314 34.674 21.74 49.509 C 20.864 57.79 23.185 65.512 27.62 71.634 C 32.495 78.366 36.208 85.859 39.432 93.506 L 41.34 97.976 L 67.465 97.925 L 70.456 90.813 C 73.242 84.183 76.517 77.705 80.798 71.914 C 84.744 66.655 87.091 60.076 87.091 52.938 Z" fill="rgb(255,255,255)"></path>
      <path d="M 57.974 120 L 50.882 120 C 44.822 120 39.896 115.174 39.896 109.204 L 39.87 100.212 C 39.87 98.865 40.979 97.773 42.346 97.773 L 66.433 97.722 C 67.8 97.722 68.909 98.815 68.909 100.161 L 68.935 109.153 C 68.935 115.148 64.035 119.975 57.974 120 Z" fill="#181818"></path>
    </svg>
  )
}

 class InsightCardDeck extends React.Component {
  constructor(props) {
    super(props);
    this.color = this.props.color ? Color(this.props.color) : blue;
    this.dark = this.props.color ? Color.darken(this.color, 100) : darkBlue;
    this.state = {
      isOpen: false, 
      data: [], 
      toggle: false,
    }

    window.addEventListener('firebaseChanged', this.handleUpdate());
  }

  loadData() {

    
    if(store.selectedWorkshop.insights){
      let stateData = []
      store.selectedWorkshop.insights.forEach((elem,index)=>{
        let data = JSON.parse(elem)
        let card1 = data.cards[0]
        let type1 = card1[0]==="W" ? "whatif" : "invention"
        
        let card2 = data.cards[1]
        let type2 = card2[0]==="W" ? "whatif" : "invention"

        let insights = data.insights

        insights.forEach((insight, i)=>{
          if(insight) {

          
          if(insight.type === "insights") {
            for(let j=0; j<insight.insights; j++){
              if(insight.insights[j].length<5){
                insight.insights.splice(j,1)
              }
            }
          }
        }
        })

        stateData.push({type: type1, sentence: card1},{type: type2, sentence: card2},  {type: "insights", insights: insights})  // let data = JSON.parse(elem)



      })


      // Add temporary insights

      if(store.insights && store.insights.length>0){

        store.insights.forEach((insight)=>{
          stateData.push({type: insight.type, sentence: insight.sentence})  // let data = JSON.parse(elem)

        })

      }
      

      this.setState({data: stateData})
        this.setState({isOpen: true})

      // setTimeout(()=>{
      //   this.setState({isOpen: true})

      // }, 1000)
    } else {

      if(store.insights && store.insights.length>0) {
        this.setState({isOpen:true})
          let stateData = []
          store.insights.forEach((insight)=>{
            stateData.push({type: insight.type, sentence: insight.sentence})  // let data = JSON.parse(elem)
  
          })
  
         
  
        this.setState({data: stateData})

      } else {
        this.setState({isOpen:false})

      }
    }
  }

  handleUpdate() {
        console.log('firebase state changed');
        this.setState({isOpen: false})
        this.loadData()
   
    
  }

  componentDidMount() {



    this.loadData()

  }

  componentWillUnmount() {
    window.removeEventListener('insightsUpdated', this.handleUpdate())
  }


  handleCardSave(index) {

  }
  render() {
    const { breakpoints, currentBreakpoint } = this.props

    if(!this.state.isOpen) {
      return <AreThereInsights></AreThereInsights>
    } else {
      console.log(breakpoints[currentBreakpoint])
      if(breakpoints[currentBreakpoint] > breakpoints.tablet) {

    return (
      <div className="cards-content">
      <Page
        width={"83%"}
        alignment="center"
        height={"78%"}

        // scaleX={"0.8"}
        left={"7.5%"}
        overflow={"visible"}
        style={pageStyle}
        initial={"closed"}
        animate={ "open" }
        variants={variants}
        transition={{  delay:0.5, duration: 0.25 }}
        // gap={-50}
        effect={info => {
          const offset = info.normalizedOffset;

          const background = transform(
            offset,
            [-1, 0, 1],
            [this.dark, this.color, this.dark]
          );
          const color = transform(
            offset,
            [-1, 0, 1],
            [textNone, text, textNone]
          );

          const scaleY = transform(offset, [-1, 0, 1], [0.9, 1, 0.9]);
          const width = transform(offset, [-1, 0, 1], ["105%", "90%", "105%"]);
          // const x = transform(offset, [-1, 0, 1], [50, , -50]);

          const borderRadius = transform(offset, [-1, 0, 1], [20, 20, 20]);
          const boxShadow = transform(
            offset,
            [-1, 0, 1],
            [noShadowCard, shadowCard, noShadowCard]
          );

          return {
            color,
            background,
            borderRadius,
            scaleY,
                      
            boxShadow
          };
        }}
      >
        { this.state.data.map((data, index) => {

          
          return <InsightCard type={data.type} key={index} sentence={data.sentence?data.sentence:""} insights={data.insights?data.insights:null}/>;
        })}
      </Page>
      </div>
    );
      } else {
        return(
          <div className="cards-content">

          <Page
            width={"97%"}
            maxWidth={"200px"}
            height={"93%"}
            top={"5%"}
            alignment="center"
            // x={"2.5%"}
            overflow={"visible"}
            style={pageStyle}
            initial={"closed"}
            animate={ "open" }
            variants={variants}
            transition={{  delay:0.5, duration: 0.25 }}
            // gap={-50}
        effect={info => {
          const offset = info.normalizedOffset;

          const background = transform(
            offset,
            [-1, 0, 1],
            [this.dark, this.color, this.dark]
          );
          const color = transform(
            offset,
            [-1, 0, 1],
            [textNone, text, textNone]
          );

          const scaleY = transform(offset, [-1, 0, 1], [0.85, 1, 0.85]);
          const scaleX = transform(offset, [-1, 0, 1], [1.02, 1, 1.02]);

          const borderRadius = transform(offset, [-1, 0, 1], [20, 20, 20]);
          const boxShadow = transform(
            offset,
            [-1, 0, 1],
            [noShadowCard, shadowCard, noShadowCard]
          );

          return {
            color,
            background,
            borderRadius,
            scaleY,
            scaleX,
            boxShadow
          };
        }}
      >
        { this.state.data.map((data, index) => {

          
          return <InsightCard type={data.type} key={index} sentence={data.sentence?data.sentence:""} insights={data.insights?data.insights:null}/>;
        })}
      </Page>
      </div>
    )
      }
  }
}
}

export default withBreakpoints(InsightCardDeck)
