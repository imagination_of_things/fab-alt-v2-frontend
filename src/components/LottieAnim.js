import React from 'react'
import Lottie from 'react-lottie';
import * as CardMachine from '../animations/Hello/card-machine.json'
import * as CreateCards from '../animations/Hello/create-cards.json'
import * as DemoCards from '../animations/Hello/demo-cards.json'
import * as JoinCards from '../animations/Hello/join-cards.json'
import * as SaveInsights from '../animations/Tutorial/save-insights.json'
import * as Swipe from '../animations/Tutorial/swipe.json'
import * as TapHold from '../animations/Tutorial/tap-hold.json'
import * as Tap from '../animations/Tutorial/tap.json'
export default class LottieAnim extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isStopped: false,
            isPaused: false
        };
        switch (props.animation) {
            case "card-machine":
                this.animationData = CardMachine

                break;
            case "create-cards":
                this.animationData = CreateCards

                break;
            case "demo-cards":
                this.animationData = DemoCards

                break;
            case "join-cards":
                this.animationData = JoinCards
                break;
            case "card-machine":
                this.animationData = CardMachine

                break;
            case "save":
                this.animationData = SaveInsights

                break;
            case "swipe":
                this.animationData = Swipe

                break;
            case "tap-hold":
                this.animationData = TapHold

                break;
            case "tap":
                this.animationData = Tap

                break;
            default:
                break;
        }
    }

    render() {
        const buttonStyle = {
            display: 'block',
            margin: '10px auto'
        };

        const defaultOptions = {
            loop: true,
            autoplay: true,
            animationData: this.animationData.default,
            rendererSettings: {
                // preserveAspectRatio: 'xMidYMid slice'
            }
        };

        return ( <div>
            
            <Lottie
                options={defaultOptions}
                height={this.props.height
                ? this.props.height
                : 100}
                width={this.props.width
                ? this.props.width
                : 100}
                isStopped={this.state.isStopped}
                isPaused={this.state.isPaused} />
            </div>
        )
    }
}