import * as React from "react";
import { motion } from "framer-motion";

const Path = props => (
  <motion.path
    fill="transparent"
    strokeWidth="3"
    stroke="white"
    strokeLinecap="round"
    {...props}
  />
);

export const MenuToggle = ({ toggle }) => (
  <button className="menu-button" onClick={toggle}>
    <svg width="14" height="23" viewBox="0 0 23 23">
      <Path
        variants={{
          closed: { d: "M 2 6.5 L 20 6.5" },
          open: { d: "M 3 16.5 L 17 2.5" }
        }}
      />

      <Path
        variants={{
          closed: { d: "M 2 16.346 L 15 16.346" },
          open: { d: "M 3 2.5 L 17 16.346" }
        }}
      />
    </svg>
  </button>
);
