import * as React from "react";
import {useRef} from "react";
import {motion, useCycle, useAnimation} from "framer-motion";
import {useDimensions} from "./use-dimensions";
import {MenuToggle} from "./MenuToggle";
import zIndex from "@material-ui/core/styles/zIndex";
import "../styles/main.css"
import QRCode from 'qrcode.react'
import {store} from 'react-recollect'
import WebShareButton from "./WebShareButton";
import {Media} from 'react-breakpoints'
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';

const Path = props => (<motion.path
    fill="transparent"
    strokeWidth="3"
    stroke="white"
    strokeLinecap="round"
    {...props}/>);

export const ShareModal = (props) => {
    const [isOpen,
        toggleOpen] = useCycle(false, true);

    const [open, setOpen] = React.useState(false);

    const [url,
        setUrl] = React.useState("https://fabalternatives.design/tool/")
    let variants = {
        visible: {
            clipPath: `circle(${ 1000 * 2 + 200}px at 40px 40px)`,
            transition: {
               duration: 0.5
            },
            opacity: 1,
            zIndex: 8

        },
        hidden: {
            clipPath: `circle(0px at ${window.innerWidth*0.9}px  ${window.innerHeight*0.05}px)`,
            transition: {
                duration: 0.5
            },
            opacity: 1,
            transitionEnd: {
                zIndex: 0
            }
        }
    }

    if (window.innerWidth >= 1200) {
        variants.hidden.clipPath = `circle(0px at ${ 0}px  ${window.innerHeight}px)`;
    }

    const shareAPI = () => {
        if (navigator.share !== undefined) {
            navigator
                .share({title: "Fabricating Alternatives Workshop", url: url})
                .then(() => {})
                .catch((error) => {
                    console.log(error)
                })
        }
    }

    const handleClick = () => {
        setOpen(true);
      };
    
      const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
      };

    const controls = useAnimation()
    
    function addToClipboard(){
        navigator.clipboard.writeText(url).then(()=> {
            console.log('Async: Copying to clipboard was successful!');
            
          }, function(err) {
            console.error('Async: Could not copy text: ', err);
          });
    }

    React.useEffect(() => {
        console.log(props.toggle)
        setUrl("https://fabalternatives.design/tool/?" + (store.selectedWorkshop
            ? store.selectedWorkshop.id
            : "fxqu"))

    }, [props.toggle],);

    return (
        <motion.div
            className="share-modal"
            animate={props.toggle
            ? "visible"
            : "hidden"}
            transition={{
            duration: 1
        }}
            variants={variants}>
            <div className="share-top-space">
                <button
                    className="menu-button-share"
                    style={{
                    height: "100%",
                    zIndex: 12
                }}
                    onClick={() => {
                    props.clickHandler()
                }}>
                    <svg width="14" height="14" viewBox="0 0 23 23">
                        <Path d={"M 3 16.5 L 17 2.5"}/>

                        <Path d={"M 3 2.5 L 17 16.346"}/>
                    </svg>
                </button>

            </div>
            <div className="share-qr-code">
                <div style={{color:"lightgrey"}}>Session</div>
                <div className="text-medium" style={{width: "80%",marginTop:"-15px", textAlign:"center"}}>{store.selectedWorkshop.title}</div>

                <QRCode value={url}/>
                <div className="text-medium">
                    Your code is <span style={{
                        color: "red"
                    }}>{store.selectedWorkshop
                            ? store.selectedWorkshop.id
                            : "null"}</span>
                </div>
            </div>

            <div className="share-text-box">
                Anyone can scan this to join your card deck.
                <br/>
                Or you can
                <br/>
                <Media>
                    {({breakpoints, currentBreakpoint}) => breakpoints[currentBreakpoint] > breakpoints.mobileLandscape
                        ? (
                            <>
                            <button
                                className="share-send-a-link"
                                onClick={() => {
                                    setOpen(true)
                                addToClipboard()
                            }}>
                                Copy Link
                            </button>
                            <Snackbar
                            anchorOrigin={{
                              vertical: 'bottom',
                              horizontal: 'left',
                            }}
                            open={open}
                            autoHideDuration={6000}
                            onClose={handleClose}
                            message="Link copied to clipboard"
                            action={
                              <React.Fragment>
                              
                                <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                                  <CloseIcon fontSize="small" />
                                </IconButton>
                              </React.Fragment>
                            }
                          />
                          </>
                        )
                        : (
                            <motion.button
                                className="share-send-a-link"
                                whileTap={{scale:0.9}}
                                onClick={() => {
                                shareAPI();
                            }}>
                                Send a Link
                            </motion.button>
                        )}
                </Media>

            </div>

        </motion.div>
    )
}

const itemIds = [0, 1, 2, 3, 4];
