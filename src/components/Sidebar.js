import * as React from "react";
import {useRef} from "react";
import {motion, useCycle} from "framer-motion";
import {useDimensions} from "./use-dimensions";
import {Link} from 'react-router-dom'
import {MenuToggle} from "./MenuToggle";
import zIndex from "@material-ui/core/styles/zIndex";
import Button from "@material-ui/core/Button";
import {makeStyles} from '@material-ui/core/styles';
import {store} from 'react-recollect'

const useStyles = makeStyles({
    root: {
        background: 'linear-gradient(45deg, #2862ff 30%, #2862ff 90%)',
        borderRadius: 3,
        border: 0,
        color: 'white',
        height: 48,
        padding: '0 30px',
        marginBottom: "30px"
    }
});


const sidebar = {
    open: (height = 1000) => ({
        clipPath: `circle(${window.innerHeight*2}px at ${window.innerWidth>1024?window.innerWidth*0.025:window.innerWidth*0.10}px  ${window.innerHeight*0.05}px)`,
        // opacity: 1,
        zIndex: 6,
        transition: {
            duration:0.5
        },

    }),
    closed: {
        clipPath: `circle(0px at ${window.innerWidth>1024?window.innerWidth*0.05:window.innerWidth*0.14}px  ${window.innerHeight*0.05}px)`,
        // opacity:0.8,
        transition: {
            // delay: 0.5,
            duration:0.5

        },
        transitionEnd: {
            zIndex:0
          },

    }
};

const infoPanelVariants = {
    visible: {
        opacity: 1,
        display: "flex",
        transition: {
            type: "spring",
            stiffness: 20,
            restDelta: 2
        }
    },
    hidden: {
        opacity: 0,
        display: "none",
        transition: {
            type: "spring",
            stiffness: 400,
            damping: 40
        }
    }
}
export const Sidebar = (props) => {
    const classes = useStyles();

    const [isOpen,
        toggleOpen] = useCycle(false, true);
    const containerRef = useRef(null);
    const {height} = useDimensions(containerRef);
    const [infoOpen,
        setInfoOpen] = React.useState(true);

    const [checkOpen,
        setCheckOpen] = React.useState(false);

        
    return (
        <motion.nav
            initial={false}
            animate={isOpen
            ? "open"
            : "closed"}
            custom={height}
            ref={containerRef}>
            <motion.div className="background" variants={sidebar}>
                <motion.div
                    className="info-panel"
                    variants={infoPanelVariants}
                    animate={infoOpen
                    ? "visible"
                    : "hidden"}>
                    <div className="info-title">
                        {store.selectedWorkshop
                            ? store.selectedWorkshop.title
                            : "Automation"}
                        <p className="text-small">Made from these sources:</p>
                    </div>
                    <div className="info-sources">
                        <a href={store.selectedWorkshop ? store.selectedWorkshop.urls[0]:""} target="_blank">
                        <motion.div whileHover={{color:"#ff5133"}} className="info-sources-text">
                            {store.selectedWorkshop
                                ? store.selectedWorkshop.meta[0]
                                : "https://google.com"}
                        </motion.div>
                        </a>
                        <a href={store.selectedWorkshop ? store.selectedWorkshop.urls[1]:""} target="_blank">

                        <motion.div whileHover={{color:"#ff5133"}} className="info-sources-text">
                            {store.selectedWorkshop
                                ? store.selectedWorkshop.meta[1]
                                : "https://google.com"}
                        </motion.div>
                        </a>
                        <a href={store.selectedWorkshop ? store.selectedWorkshop.urls[2]:""} target="_blank">

                        <motion.div whileHover={{color:"#ff5133"}} className="info-sources-text">
                            {store.selectedWorkshop
                                ? store.selectedWorkshop.meta[2]
                                : "https://google.com"}
                        </motion.div>
                        </a>

                    </div>
                    <div className="info-learn">
                        <motion.div      whileHover={{ scale: 1.025 }} className="info-learn-text">
                           <a href="http://fabalternatives.design" style={{color:"lightgrey"}} target="_blank"> More about Fabricating Alternatives &#8594;</a>
                           

                        </motion.div>
                        <motion.div  style={{marginTop:"10px"}}    whileHover={{ scale: 1.025 }} className="info-learn-text">

                        <a href="https://imaginationofthings.com"  target="_blank"> Created by Imagination of Things</a>
                        </motion.div>
                    </div>

                    <div className="info-bottom">

                        <motion.button
                            onClick={() => {
                            setInfoOpen(false);
                            setCheckOpen(true)
                        }}
                            whileTap={{
                            scale: 0.9
                        }}
                            className="info-button">
                            New Session
                        </motion.button>

                    </div>

                </motion.div>

                <motion.div
                    className="info-panel"
                    animate={checkOpen
                    ? "visible"
                    : "hidden"}
                    variants={infoPanelVariants}>
                    <div className="info-title">
                        Are you sure you want to leave?
                        <p className="text-small">If you don't note down the share code, you won't be able to revisit this session</p>
                    </div>
                    <div className="info-bottom-box">
                    <Link to="/">

                        <Button
                            variant="contained"
                            classes={{
                            root: classes.root,
                            label: classes.label, // class name, e.g. `classes-nesting-label-x`

                            }} >
                        Yes, try another!
                        </Button>
                    </Link> 

                    <Button size={"small"} onClick={()=>{toggleOpen()}}>No, I want to stay here.</Button>


                    </div>
                </motion.div>

            </motion.div>

            <MenuToggle
                toggle={() => {
                toggleOpen();
                setCheckOpen(false);
                setInfoOpen(true)
            }}/>
        </motion.nav>
    );
};
