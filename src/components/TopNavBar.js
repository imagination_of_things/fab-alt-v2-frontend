import * as React from "react";

import '../styles/main.css'
// import {Sidebar} from './Sidebar'
import {useRef} from "react";
import {motion, useCycle} from "framer-motion";
import {useDimensions} from "./use-dimensions";
import {MenuToggle} from "./MenuToggle";



function PaperPlane() {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="20">
            <path d="M 0 5.185 L 33.622 0.741 L 4.533 9.259 Z" fill="rgb(255, 255, 255)"></path>
            <path
                d="M 4.533 9.259 L 4.533 16.667 L 33.622 0.741 Z"
                fill="hsla(0, 0%, 100%, 0.51)"></path>
            <path d="M 9.444 14.074 L 17 20 L 33.244 0.741 Z" fill="rgb(255, 255, 255)"></path>
        </svg>
    )
}

export function TopNavigationBar(props) {

    return (

        <div className="cards-top-bar">
            <div className="cards-top-bar-menu">

                
            </div>
            <div className="cards-top-bar-divider"></div>
            <motion.div
                whileTap={{
                scale: 0.9
            }}
                onClick={props.shareToggle}
                className="cards-top-bar-share">
                <PaperPlane></PaperPlane>
            </motion.div>

        </div>

    )
}