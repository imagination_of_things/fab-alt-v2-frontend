import { useState, useEffect } from 'react';

export default function useLongPress(callback = () => {}, ms = 300) {
  const [startLongPress, setStartLongPress] = useState(false);

  useEffect(() => {
    let timerId;
    if (startLongPress) {
      timerId = setTimeout(callback, ms);
    } else {
      clearTimeout(timerId);

    }

    return () => {
      clearTimeout(timerId);
    };
  }, [startLongPress]);

  return {
    // onClick:()=>setStartLongPress(true),
    onMouseDown: () => {
      setStartLongPress(true)
      console.log("mouseDown")
    },
    onMouseUp: () => {setStartLongPress(false)
      console.log("mouseUp")
    },
    onMouseLeave: () => setStartLongPress(false),
    onTouchStart: () => setStartLongPress(true),
    onTouchEnd: () => setStartLongPress(false),
  };
}