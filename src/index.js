import React from "react";
import ReactDOM from "react-dom";
import {HashRouter as Router, Switch, Route, Link} from "react-router-dom";
import {ThemeProvider, createMuiTheme} from "@material-ui/core/styles";
import {theme} from "./styles/MuiTheme";
import App from "./App";
import ReactBreakpoints from 'react-breakpoints'

const breakpoints = {
  mobile: 320,
  mobileLandscape: 480,
  tablet: 768,
  tabletLandscape: 1024,
  desktop: 1200,
  desktopLarge: 1500,
  desktopWide: 1920,
}
const rootElement = document.getElementById("root");
ReactDOM.render(
    <Router>
    <ThemeProvider theme={theme}>
    <ReactBreakpoints breakpoints={breakpoints}>

        <App/>
        </ReactBreakpoints>
    </ThemeProvider>
</Router>, rootElement);
