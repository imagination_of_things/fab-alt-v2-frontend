// utils/API.js

import axios from "axios";
const demoURL = "http://0.0.0.0:8080/api"
const prodURL = "https://fabalt-idvgffrwca-ez.a.run.app/api"
export default axios.create({
  baseURL: prodURL,
  // responseType: "json"
});