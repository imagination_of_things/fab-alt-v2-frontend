import {store} from 'react-recollect';
import API from "../../services/API";

export const addInsight = async( insight) => {
    store.loading = true;
    let _type = insight.charAt(0)==="W"?"whatif":"invention"

    if (!store.insights) {
        store.insights = []
    }

    if(store.insights.includes(insight) === false) {
        store.insights.push({type: _type, sentence:insight})

    }

    if (store.insights.length == 2) {

       fetchInsights(store.id).then(()=>{
           console.log("Success")

        if(store.insights.length>2){
            store.insights.splice(0,2)

        } else {
            store.insights = []
        }

       }).catch(()=>{
           console.log("error with insights")
       })
    }



    console.log(store.insights, store.insights.length)



};

async function fetchInsights(id) {

    return new Promise((resolve, reject) => {

        if (store.insights && store.insights.length >= 2) {

            if(store.insights[0].sentence === store.insights[1].sentence) {
                store.insights.splice(1,1)
                resolve()
            }

            const insightRequest = {
                id: id,
                prefix1: store.insights[0].sentence,
                prefix2: store.insights[1].sentence
            }

            console.log(insightRequest)

            try {
                const response = API.post('/insight', JSON.stringify(insightRequest)).then((response)=>{
                    // console.log(ins)
                    console.log(response)

                    resolve()
                });
            

            } catch (e) {
                console.log(`😱 Axios request failed: ${e}`);
                reject()
            }
        } else {
            reject()
        }
    })
}