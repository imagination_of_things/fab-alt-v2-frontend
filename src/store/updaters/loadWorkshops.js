import {store} from 'react-recollect';

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

export const loadDefault = async(workshopRef, id) => {
    store.loading = true;
    store.workshopRef = workshopRef;
    store.id = id


    return new Promise((resolve, reject) => {
        workshopRef
            .doc(id)
            .get()
            .then(function (doc) {
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    let {
                        id,
                        inventions,
                        problem,
                        title,
                        urls,
                        whatif,
                        insights,
                        meta,
                        words
                    } = doc.data();
                    inventions = shuffle(inventions)
                    whatif = shuffle(whatif)
                    store.selectedWorkshop = {
                        id: id,
                        inventions: inventions,
                        problem: problem,
                        title: title,
                        urls: urls,
                        whatif: whatif,
                        words: words,
                        meta: meta,
                        insights: insights
                    }
                    store.loading = true;
                    resolve()
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                    store.selectedWorkshop = null
                    store.loading = false;
                    reject()


                }
            })
            .catch(function (error) {
                console.log("Error getting document:", error);
                reject();
                store.loading = false;

            });
    })

};



export const loadWorkshopFromID = async(id) => {
    store.loading = true;
    store.id = id

    return new Promise((resolve, reject) => {
        store
            .workshopRef
            .doc(id)
            .get()
            .then(function (doc) {
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    let {
                        id,
                        inventions,
                        problem,
                        title,
                        urls,
                        whatif,
                        words, 
                        meta,
                        insights
                    } = doc.data();
                    inventions = shuffle(inventions)
                    whatif = shuffle(whatif)
                    if(insights) {
                        insights = insights.filter(function(val) { return val !== null; })

                    }
                    store.selectedWorkshop = {
                        id: id,
                        inventions: inventions,
                        problem: problem,
                        title: title,
                        urls: urls,
                        whatif: whatif,
                        words: words,
                        meta: meta,
                        insights: insights
                    }
                    store.loading = false;
                    resolve("found")
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                    store.selectedWorkshop = null
                    store.loading = false;

                    reject("no workshop")
                }
            })
            .catch(function (error) {
                console.log("Error getting document:", error);
                store.loading = false;

                reject("no workshop")

            });

    })

};

export const updateWorkshop = async() => {
    store.loading = true;

    return new Promise((resolve, reject) => {
        store
            .workshopRef
            .doc(store.id)
            .get()
            .then(function (doc) {
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    let {
                        id,
                        inventions,
                        problem,
                        title,
                        urls,
                        whatif,
                        words, 
                        meta,
                        insights
                    } = doc.data();
                    inventions = shuffle(inventions)
                    whatif = shuffle(whatif)
                    if(insights) {
                        insights = insights.filter(function(val) { return val !== null; })

                    }
                    store.selectedWorkshop = {
                        id: store.id,
                        inventions: inventions,
                        problem: problem,
                        title: title,
                        urls: urls,
                        whatif: whatif,
                        words: words,
                        meta: meta,
                        insights: insights
                    }
                    store.loading = false;
                    resolve("found")
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                    // store.selectedWorkshop = null
                    // store.loading = false;

                    reject("no workshop")
                }
            })
            .catch(function (error) {
                console.log("Error getting document:", error);
                store.loading = false;

                reject("no workshop")

            });

    })

};