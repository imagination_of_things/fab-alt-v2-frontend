import React from "react";
import {
    Frame,
    Stack,
    Page,
    Color,
    useAnimation,
    useCycle
} from "framer";
import {appLayout} from './AppLayout'


export function ColorWrap(props) {
    React.useEffect(() => {
        
        handleChange(props.stage);
        console.log("Body Stack", props.stage);
      });
    const variants = {
        home: {
            backgroundColor: "#181818",
            transition: {
                duration: 1.0
            }
        },
        first: {
            backgroundColor: "#2a62ff",
            transition: {
                duration: 1.0
            }
        },
        second: {
            backgroundColor: "#d60201",
            transition: {
                duration: 1.0
            }
        },
        third: {
            backgroundColor: "#c4c219",
            transition: {
                duration: 1.0
            }
        }
    };

    const controls = useAnimation();
    const handleChange = newValue => {
      console.log(newValue);
      if (newValue === 0) {
        // history.push("/whatif");
  
        controls.start("home");
      } else if (newValue === 1) {
        // history.push("/inventions");
        controls.start("first");
      } else if (newValue === 2) {
        // history.push("/favourites");
        controls.start("second");
      } else if (newValue === 3) {
        // history.push("/favourites");
        controls.start("third");
      }
    };

    return (

        <Frame {...appLayout} initial="home" animate={controls} variants={variants}>
            {props.children}
        </Frame>
    )
}