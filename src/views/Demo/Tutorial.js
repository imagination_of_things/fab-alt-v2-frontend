import * as React from "react";
import {motion, MotionContext} from "framer-motion";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import LottieAnim from '../../components/LottieAnim'
import {Media} from 'react-breakpoints'

import "../../styles/main.css"
import {
    Close,

} from './Graphics'

import {css} from "@emotion/core";
// Another way to import. This is recommended to reduce bundle size
import ScaleLoader from "react-spinners/ScaleLoader";

// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css `
  display: block;
  margin: 0 20px;
  border-color: white;
`;
const textField = {
    width: "100%",
    maxWidth: "500px",
    padding: "5px 0"
};

const textFieldDesktop = {
    width: "80vw",
    maxWidth: "500px",
    padding: "5px 0"
}

const topBar = {
    width: "80%",
    height: "10%",
    overflow: "visible"
}

function TopBar() {
    return (
        <motion.div className="setup-top-bar">
            <Link to="/">
                <Close></Close>

            </Link>
        </motion.div>

    )
}


export function Page4(props) {
    //   const classes = useStyles();
    const list = {
        visible: {
            opacity: 1,
            transition: {
                when: "beforeChildren",
                staggerChildren: 0.1
            }
        },
        hidden: {
            opacity: 0,
            transition: {
                when: "afterChildren"
            }
        }
    }

    const item = {
        visible: {
            opacity: 1,
            x: 0
        },
        hidden: {
            opacity: 0,
            x: -100
        }
    }
    return (
        <motion.div
            className="setup-page4"
            initial="hidden"
            animate="visible"
            variants={list}>
            <TopBar></TopBar>
            <div className="tutorial-content">
                <motion.div className="setup-item-stack" variants={item}>
                    <Media>
                    {({ breakpoints, currentBreakpoint }) => (
                <LottieAnim animation={"swipe"} width={window.innerWidth>1024?50 * 4:100} height={window.innerHeight/6}></LottieAnim>
                    )}
                </Media>
                    <div className="setup-text">
                        <span className="text-large">
                            Swipe
                        </span><br/>
                        <span className="small-text">
                            through cards.
                        </span>
                    </div>
                </motion.div>

                <motion.div className="setup-item-stack" variants={item}>
                <LottieAnim animation={"tap"} width={window.innerWidth>1024?50 * 4:100} height={window.innerHeight/6}></LottieAnim>

                    <div className="setup-text">
                        <span className="text-large">
                        {window.innerWidth>1024?" Click":"Tap"}
                        </span><br/>
                        <span className="small-text">
                            to switch words
                        </span>
                    </div>
                </motion.div>

                <motion.div className="setup-item-stack" variants={item}>
                <LottieAnim animation={"tap-hold"} width={window.innerWidth>1024?50 * 4:100} height={window.innerHeight/6}></LottieAnim>

                    <div className="setup-text">
                        <span className="text-large">
                            {window.innerWidth>1024?" Right Click & Hold":"Tap & Hold"}
                        </span><br/>
                        <span className="small-text">
                            to edit words
                        </span>
                    </div>
                </motion.div>


                <motion.div className="setup-item-stack" variants={item}>
                <LottieAnim animation={"save"} width={window.innerWidth>1024?50 * 4:100} height={window.innerHeight/6}></LottieAnim>

                    <div className="setup-text">
                        <span className="text-large">
                            Save cards
                        </span><br/>
                        <span className="small-text">
                            to your insights deck
                        </span>
                    </div>
                </motion.div>
            </div>

            <div className="loader">
                    <button onClick={()=>{props.handler()}} className="blue-button" style={{color:"white", fontSize:'calc(16px + 6 * ((100vw - 320px) / 2000))'}}>
                        Got it!
                    </button>
                  

            </div>
        </motion.div>

    )
}