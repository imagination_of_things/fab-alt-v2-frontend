import React from 'react';
import {motion} from "framer-motion"
import {Link} from 'react-router-dom'
import {Crane, Future, Urban, Renewable, Close, Triangle} from './Graphics'

import {loadWorkshopFromID, loadDefault} from '../../store/updaters/loadWorkshops'
import './Demo.css'
import {Page4} from './Tutorial'
const list = {
    visible: {
        opacity: 1,
        transition: {
            when: "beforeChildren",
            staggerChildren: 0.1
        }
    },
    hidden: {
        opacity: 0,
        transition: {
            when: "afterChildren"
        }
    }
}

const item = {
    visible: {
        opacity: 1,
        x: 0
    },
    hidden: {
        opacity: 0,
        x: -100
    }
}


const permanentDemos = {
    Automation: 'fxqu',
    Frontiers: 'ohfe',
    Urbanization: 'ylfw',
    Climate: 'mkwc'



}

export class Demo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tutorial: true
        }
    }
    render() {


        if(this.state.tutorial) {
            return <Page4 handler={()=>{this.setState({tutorial:false})}}></Page4>
        }
        return (

            <motion.div
                className="demo-flex-container"
                initial="hidden"
                animate="visible"
                variants={list}>
                <motion.div className="demo-top-bar">
                    <div className="close-button">
                    <Link to="/">
                        <Close className="demo-close"></Close>

                    </Link>
                    </div>

                </motion.div>
                <motion.div className="demo-hello-text-box">
                    Choose a demo deck
                </motion.div>
                <div className="form-segment">
                    <Triangle></Triangle>
                </div>

                    <div className="item-layout">

              
                    <motion.div
                        className="demo-item-stack"
                        variants={item}
                        whileTap={{
                        scale: 0.9
                    }} onClick={()=>{
                        console.log(this.props.history)
                        loadWorkshopFromID(permanentDemos.Automation).then(()=>{
                            localStorage.setItem("session", JSON.stringify({time: new Date(), id: permanentDemos.Automation }))

                            this.props.history.push('/whatif')

                        })

                    }}>
                        
                        <Crane></Crane>
                        <div className="demo-text">
                            <span className="text-large">Automation</span>
                            <br/>
                            Economy and AI
                        </div>
                    </motion.div>


                <motion.div
                    className="demo-item-stack"
                    variants={item}
                    whileTap={{
                    scale: 0.9
                }} onClick={()=>{
                    console.log(this.props.history)
                    loadWorkshopFromID(permanentDemos.Frontiers).then(()=>{
                        localStorage.setItem("session", JSON.stringify({time: new Date(), id: permanentDemos.Frontiers }))

                        this.props.history.push('/whatif')

                    })

                }}>
                    <Future></Future>
                    <div className="demo-text">
                    <span className="text-large">Frontiers</span>
                            <br/>
                            Futures in Outer Space
                    </div>
                </motion.div>
                <motion.div
                    className="demo-item-stack"
                    variants={item}
                    whileTap={{
                    scale: 0.9
                }} onClick={()=>{
                    console.log(this.props.history)
                    loadWorkshopFromID(permanentDemos.Urbanization).then(()=>{
                        localStorage.setItem("session", JSON.stringify({time: new Date(), id: permanentDemos.Urbanization}))

                        this.props.history.push('/whatif')

                    })

                }}>
                    <Urban></Urban>
                    <div className="demo-text">
                    <span className="text-large">Urbanization</span>
                            <br/>
                            The Future of Living
                    </div>
                </motion.div>

                <motion.div
                    className="demo-item-stack"
                    variants={item}
                    whileTap={{
                    scale: 0.9
                }} onClick={()=>{
                    console.log(this.props.history)
                    loadWorkshopFromID(permanentDemos.Climate).then(()=>{
                        localStorage.setItem("session", JSON.stringify({time: new Date(), id: permanentDemos.Climate }))
                        this.props.history.push('/whatif')

                    })

                }}>
                    <Renewable></Renewable>
                    <div className="demo-text">
                    <span className="text-large">Ecology</span>
                            <br/>
                            Renewable Futures
                    </div>
                </motion.div>
                </div>


            </motion.div>
        );
    }
}
