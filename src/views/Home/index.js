import * as React from "react";
import {motion} from "framer-motion";
import {Switch, Route, Link} from "react-router-dom";
import "./Home.css";
import {store} from 'react-recollect'
import {Media} from 'react-breakpoints'
import LottieAnim from '../../components/LottieAnim'
const variants = {
    visible: i => ({}),
    hidden: {}
}
export function Home(props) {

    return (
        <Media>
            {({breakpoints, currentBreakpoint}) => breakpoints[currentBreakpoint] > breakpoints.mobileLandscape
                ? (
                    <motion.div id="home" className="home-flex-container-wide">
                        <motion.div className="home-stack-desktop">

                            <motion.div
                                custom={0}
                                initial="hidden"
                                animate="visible"
                                variants={variants}
                                className="home-top-content">
                                <div className="home-text">
                                    <motion.div className="card-machine-animation">
                                        <LottieAnim animation={"card-machine"} width={50 * 4} height={66 * 2.5}></LottieAnim>

                                    </motion.div>
                                    <div className="home-title">

                                        Hi there,
                                    </div>

                                    {/* <br/> */}
                                    <div className="home-subtitle" style={{textAlign:"left"}}>
                                        I am
                                        <span
                                            style={{
                                            fontWeight: "bolder"
                                        }}> Alternatif</span>, a machine that remixes & multiplies ideas and transforms them
                                        into a card deck. Let’s jump into a session together!
                                    </div>
                                </div>
                            </motion.div>
                            <motion.div
                                custom={1}
                                initial="hidden"
                                animate="visible"
                                variants={variants}
                                className="home-demo">
                                <Link to="/demo">
                                    <div className="lottie-anim">
                                        <LottieAnim width={"34%"} height={"30%"} animation={"demo-cards"}></LottieAnim>
                                    </div>
                                    <div className="home-title">Demo</div>
                                    <div className="home-subtitle">a preset session</div>
                                </Link>

                            </motion.div>
                        </motion.div>

                        <motion.div className="home-stack-desktop">
                            <motion.div
                                custom={2}
                                initial="hidden"
                                animate="visible"
                                variants={variants}
                                className="home-create-btn">
                                <Link to="/setup">
                                    <div className="lottie-anim">
                                        <LottieAnim width={"34%"} height={"30%"} animation={"create-cards"}></LottieAnim>
                                    </div>
                                    <div className="home-title">Create</div>
                                    <div className="home-subtitle">a new session</div>
                                </Link>

                            </motion.div >
                            <motion.div
                                className="home-join-btn"
                                custom={3}
                                initial="hidden"
                                animate="visible"
                                variants={variants}>
                                <Link to="/join">
                                    <div className="lottie-anim">
                                        <LottieAnim width={"34%"} height={"30%"} animation={"join-cards"}></LottieAnim>
                                    </div>
                                    <div className="home-title">Join</div>
                                    <div className="home-subtitle">a session</div>
                                </Link>
                            </motion.div >
                        </motion.div>
                    </motion.div>

                )
                : (
                    <div id="home" className="home-flex-container">
                        <div className="home-text-box">
                            <div className="home-top"/>

                            <div className="card-machine-animation">
                                <LottieAnim animation={"card-machine"} width={200} height={100}></LottieAnim>

                            </div>
                            <div className="text-large">

                                Hi there,
                            </div>

                            {/* <br/> */}
                            <span className="text-small">
                                I am
                                <span
                                    style={{
                                    fontWeight: "bold"
                                }}> Alternatif</span>, a machine that remixes & multiplies ideas and transforms them
                                into a card deck. Let’s jump into a session together!
                            </span>

                        </div>
                        <div className="home-demo">
                            <Link to="/demo">
                                    <LottieAnim width={"30%"} height={"25%"} animation={"demo-cards"}></LottieAnim>
                                <div className="home-title">Demo</div>
                                <div className="home-subtitle">a preset session</div>
                            </Link>

                        </div>
                        <div className="home-create-join">
                            <div className="home-create-btn">
                                <Link to="/setup">
                                    <div className="lottie-anim">
                                        <LottieAnim width={"60%"} height={"30%"} animation={"create-cards"}></LottieAnim>
                                    </div>
                                    <div className="home-title">Create</div>
                                    <div className="home-subtitle">a new session</div>
                                </Link>

                            </div>
                            <div className="home-join-btn">
                                <Link to="/join">
                                    <div className="lottie-anim">
                                        <LottieAnim width={"60%"} height={"20%"} animation={"join-cards"}></LottieAnim>
                                    </div>
                                    <div className="home-title">Join</div>
                                    <div className="home-subtitle">a session</div>
                                </Link>
                            </div>
                        </div>
                    </div>
                )
}
        </Media>

    );
}
