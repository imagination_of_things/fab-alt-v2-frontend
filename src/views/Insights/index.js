import * as React from "react";
import {Frame, Stack, Page, Color} from "framer";
import CardDeck from "../../components/CardDeck";
import InsightCardDeck from '../../components/InsightCardDeck'
import {BottomNavigationBar} from '../../components/BottomNavigationBar'
import '../../styles/main.css'
import {TopNavigationBar} from '../../components/TopNavBar'
import {Sidebar} from '../../components/Sidebar'
import {ShareModal} from '../../components/ShareModal'
import "./Insights.css"

import {store} from 'react-recollect'



export class Insights extends React.Component {
    constructor(props) {
        super(props);
        this.stage = this.props.toolpage;
        console.log("demo", this.stage);
        this.state = {
            toggle: false,
            showCardDeck: false, 
            data:[]
        }
    }

    componentDidMount() {

        

    }
    render() {
        return (
            <div className="cards-flex-container" >
                <TopNavigationBar
                    shareToggle={() => {
                    console.log("Share toggle", this.state.toggle);
                    this.setState({toggle: !this.state.toggle})
                }}></TopNavigationBar>
                <ShareModal toggle={this.state.toggle} clickHandler={()=>{
                    this.setState({toggle: !this.state.toggle})
                }}></ShareModal>
                <div className="cards-spacer">

                </div>
                    <InsightCardDeck></InsightCardDeck>
                    <div className="cards-spacer">
                    
                    </div>
                <div className="cards-bottom-bar">
                    <BottomNavigationBar page={2} shareToggle={(value)=>{
                                            this.setState({toggle: false})

                    }}></BottomNavigationBar>

                </div>
            </div>
        )
    }
}
