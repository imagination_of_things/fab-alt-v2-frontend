import * as React from "react";
import {Frame, Stack, Page, Color} from "framer";
import CardDeck from "../../components/CardDeck";
// import RiTa from "rita";
import axios from "axios";
import {whatifCards, inventionCards, problemCards, content} from "../../styles/AppLayout";
import {BottomNavigationBar} from '../../components/BottomNavigationBar'
import '../../styles/main.css'
import {TopNavigationBar} from '../../components/TopNavBar'
import {Sidebar} from '../../components/Sidebar'
import {ShareModal} from '../../components/ShareModal'
export class Inventions extends React.Component {
    constructor(props) {
        super(props);
        this.stage = this.props.toolpage;
        console.log("demo", this.stage);
        this.state = {
            toggle: false
        }
    }

    componentWillMount() {}
    render() {
        return (
            <div className="cards-flex-container " onContextMenu={(e)=> e.preventDefault()}>
                <TopNavigationBar
                    shareToggle={() => {
                    console.log("Share toggle", this.state.toggle);
                    this.setState({toggle: !this.state.toggle})
                }}></TopNavigationBar>
                <ShareModal toggle={this.state.toggle} clickHandler={()=>{
                    this.setState({toggle: !this.state.toggle})
                }}></ShareModal>
                <div className="cards-spacer"></div>
                
                <CardDeck color={"#ff5131"} dark={30} id={"invention"}/>
                <div className="cards-spacer"></div>

                
                <div className="cards-bottom-bar">
                    <BottomNavigationBar page={1} shareToggle={(value)=>{
                                            this.setState({toggle: false})

                    }}></BottomNavigationBar>

                </div>
            </div>
        )
    }
}
