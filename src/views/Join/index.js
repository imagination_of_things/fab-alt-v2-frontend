import * as React from "react";
import {Frame, Stack} from "framer";
import {Switch, Route, Link} from "react-router-dom";
import "./join.css";
import {store} from 'react-recollect'
import {motion} from "framer-motion"
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {makeStyles} from '@material-ui/core/styles';
import {loadWorkshopFromID} from '../../store/updaters/loadWorkshops'
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
// import Button from '@material-ui/core/Button';
import {Close, JoinGraphic, Triangle} from './Graphics'
import zIndex from "@material-ui/core/styles/zIndex";

const textField = {
    width: "80vw",
    maxWidth: "300px",
    
    // marginTop: "10%"
    // height: "100",

}



const useStyles = makeStyles({
    root: {
        background: 'linear-gradient(45deg, #2862ff 30%, #2862ff 90%)',
        borderRadius: 3,
        border: 0,
        color: 'white',
        height: 48,
        padding: '0 30px',
        marginBottom: "30px"
    }
});

export function Join(props) {
    const classes = useStyles();
    const checkCode = (code) => {
        if(code.length>4) {
            return false;
        } else {
            return true;
        }
    }
    const [alertMessage, setAlertMessage] = React.useState("That code wasn't found, please try another")

    const [code, setCode] = React.useState("")
    const codeChanged = (event, value) => {
        console.log(event.target.value)
        setCode(event.target.value)

    };

    const [open, setSnack] = React.useState(false)


    
    const handleClose = () => {
      setSnack(false)
    };

    const handleClick = () => {
        console.log(code)

        loadWorkshopFromID(code.toLowerCase()).then((message)=>{
            setAlertMessage("Found workshop 👍")
            setSnack(true)
            localStorage.setItem("session", JSON.stringify({time: new Date(), id: code.toLowerCase() }))
            props.history.push("/whatif")


        }).catch((message)=>{
            setAlertMessage("No workshop was found for that code, please try another")
            setSnack(true)
            
        })


    }
    return (
        <>
        <div className="join-flex-container">
            <motion.div className="join-top-bar">
                <Link to="/">
                    <Close></Close>

                </Link>
            </motion.div>
            <div className="join-anim-frame">
                <JoinGraphic></JoinGraphic>
            </div>
            <div className="join-text-box">
                <span className="text-large">Hello! Were you invited to a live session?</span>
                Put in the four letter code to enter
            </div>
            <div className="arrow-segment">
                <Triangle></Triangle>
            </div>
            <div className="join-code-box">
                    <TextField
                        required
                        id="standard-error-helper-text"
                        helperText="4 letter code"
                        style={textField}
                        error={code.length>4}
                        onChange={codeChanged}

                        className="setup-textfield"
                        placeholder="xxxx"/>
               
            </div>
            <div className="join-bottom-box">
                <Button
                    variant="contained"
                    classes={{
                    root: classes.root,
                    label: classes.label, // class name, e.g. `classes-nesting-label-x`

                    }}
                    
                    onClick={()=>{
                        handleClick();
                    }}>
                    Enter
                </Button>
                    <Link to="/">
                    <Button size={"small"} color="primary">Back</Button>

                    </Link>

            </div>

             
        </div>
               <Snackbar
               anchorOrigin={{
                 vertical: 'bottom',
                 horizontal: 'center',
               }}
               open={open}
               onClose={handleClose}
               message={alertMessage}
               // style={{zIndex: 123123}}
               autoHideDuration={6000}
               action={
                 <React.Fragment>
                 
                   <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                     <CloseIcon fontSize="small" />
                   </IconButton>
                 </React.Fragment>
               }
             />
             </>
    );
}

// function Hello(props) {   const moveToPage = page => {     props.handler("",
// page);   };   return (   ); }