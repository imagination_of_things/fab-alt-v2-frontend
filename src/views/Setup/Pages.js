import * as React from "react";
import {motion, MotionContext} from "framer-motion";
import Button from "@material-ui/core/Button";

import {Switch, Route, Link} from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import LottieAnim from '../../components/LottieAnim'
import {Media} from 'react-breakpoints'

import {isValidUrl} from './utils'
import "../../styles/main.css"
import "./setup.css"
import {
    Cards_Setup,
    Close,
    Setup_Page2,
    Setup_Page3,
    SetupLoading1,
    SetupLoading2,
    SetupLoading3
} from './Graphics'
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import {css} from "@emotion/core";
// Another way to import. This is recommended to reduce bundle size
import ScaleLoader from "react-spinners/ScaleLoader";

// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css `
  display: block;
  margin: 0 20px;
  border-color: white;
`;
const textField = {
    width: "100%",
    maxWidth: "500px",
    padding: "5px 0"
};

const textFieldDesktop = {
    width: "80vw",
    maxWidth: "500px",
    padding: "5px 0"
}

const topBar = {
    width: "80%",
    height: "10%",
    overflow: "visible"
}

function TopBar() {
    return (
        <motion.div className="setup-top-bar">
            <Link to="/">
                <Close></Close>

            </Link>
        </motion.div>

    )
}

export function PageIndicator(props) {
    const [stage,
        setStage] = React.useState(0);
    function handleClick(val) {
        // console.log(val);
        props.handler(val);
    }

    return (
        <div className="setup-dotstyle">
            <motion.div className="setup-dotbutton">
                <motion.div
                    className="setup-dot"
                    whileTap={{
                    scale: 0.8
                }}
                    style={{
                    opacity: props.index == 0
                        ? 1
                        : 0.45
                }}
                    onClick={() => {
                    handleClick(1);
                }}/>
            </motion.div>
            <motion.div className="setup-dotbutton">
                <motion.div
                    className="setup-dot"
                    whileTap={{
                    scale: 0.8
                }}
                    style={{
                    opacity: props.index == 1
                        ? 1
                        : 0.45
                }}
                    onClick={() => {
                    handleClick(2);
                }}/>
            </motion.div>
            <motion.div className="setup-dotbutton">
                <motion.div
                    className="setup-dot"
                    whileTap={{
                    scale: 0.8
                }}
                    style={{
                    opacity: props.index == 2
                        ? 1
                        : 0.45
                }}
                    onClick={() => {
                    handleClick(3);
                }}/>
            </motion.div>
        </div>
    );
}

export function Page1(props) {

    const [projectTitle,
        setProjectTitle] = React.useState("");
    const handleChange = (event, value) => {
        console.log(event.target.value);
        setProjectTitle(event.target.value);
        props.setTitle(event.target.value);
    };
    const moveToPage = val => {
        props.handler(val);
    };
    const submitTitle = () => {
        props.handler(2);
    };
    return (
        <div className="flex-container page-1">
            <TopBar></TopBar>
            <div className="setup-anim-frame">
                <Cards_Setup></Cards_Setup>
            </div>
            <div className="setup-text-box">
                <span className="text-large">Let's Create</span>
                <br/>
                In a session you can setup a card deck for ideation and other playful activities.
                <br/>
                <br/>
                First, give a title to your session.
            </div>

            <div className="setup-text-box">
                    <TextField

                        style={textField}
                        id="standard-error-helper-text"
                        label=""
                        onChange={handleChange}
                        className="setup-textfield"
                        value={props.title}
                        helperText="Project Title"

                        placeholder="The future of..."/>
            </div>

            <div className="setup-bottom-section">

                <PageIndicator index={0} handler={moveToPage}/>

                <div className="setup-button">
                    <Button
                        variant="contained"
                        color="secondary"
                        size="small"
                        disabled={props.title.length < 5}
                        onClick={submitTitle}>
                        Next
                    </Button>
                </div>
            </div>
        </div>
    );
}

export function Page2(props) {
    const [projectDescription,
        setProjectDescription] = React.useState(props.problem
        ? props.problem
        : "");
    const handleChange = (event, value) => {
        console.log(event.target.value);
        setProjectDescription(event.target.value);
        props.setProblem(projectDescription);
    };
    const moveToPage = val => {
        props.handler(val);
    };
    const submitDescription = () => {
        props.setProblem(projectDescription)
        props.handler(3);
    };
    return (
        <div className="flex-container page-1">
            <TopBar></TopBar>

            <div className="setup-anim-frame">
                <Setup_Page2></Setup_Page2>
            </div>
            <div className="setup-text-box">

            Next, share with me your purpose in this session. What are the opportunities for change in this topic?
            </div>

            <div className="setup-text-box">
                <form noValidate autoComplete="off">

                    <TextField
                    
                        color="primary"
                        id="filled-multiline-static"
                        multiline
                        style={textField}
                        onChange={handleChange}
                        rows="6"
                        value={projectDescription}
                        helperText="Description"

                        placeholder="I want to explore..."
                        variant="filled"/>

                </form>
            </div>
            <div className="setup-bottom-section">

                <PageIndicator index={1} handler={moveToPage}/>

                <div className="setup-button">
                    <Button
                        variant="contained"
                        color="secondary"
                        disabled={projectDescription.length < 10}
                        onClick={submitDescription}
                        size="small">
                        Next
                    </Button>
                </div>
            </div>

        </div>
    );
}
export function Page3(props) {

    const [t2,
        setT2] = React.useState(props.urls[1]
        ? true
        : false)
    const [t3,
        setT3] = React.useState(props.urls[2]
        ? true
        : false)

    const [url1,
        setUrl1] = React.useState(props.urls[0]
        ? props.urls[0]
        : "");
    const [url2,
        setUrl2] = React.useState(props.urls[1]
        ? props.urls[1]
        : "");
    const [url3,
        setUrl3] = React.useState(props.urls[2]
        ? props.urls[2]
        : "");

    const setURL = (idx, value) => {

        console.log("url value", value)
        if (idx === 0) {
            setUrl1(value);
        } else if (idx === 1) {
            setUrl2(value);
        } else if (idx === 2) {
            setUrl3(value);
        }

        props.setUrls([url1, url2, url3]);
    };

    const submitNewWorkshop = () => {
        setUrl1(document.getElementById("url-field-1").value);
        setUrl2(document.getElementById("url-field-2").value);
        setUrl3(document.getElementById("url-field-3").value);

        if (isValidUrl(url1)[1] || url1.length === 0) {
            props.setUrls([url1, url2, url3]);
            props.handleSubmit([url1, url2, url3]);
        } else {
            alert("There is something wrong with the info you have submitted")
        }
    };

    const moveToPage = val => {
        // console.log(val);
        props.handler(val);
    };
    return (
        <div className="flex-container page-1">

            <TopBar></TopBar>

            <div className="setup-anim-frame">
                <Setup_Page3></Setup_Page3>

            </div>
            <div className="setup-text-box">
            Last, we need to fill my brain with relevant articles that are references and inspiration.
            </div>
            <div
                style={{
                width: "80%",
            }}>

                <div className="form">

                    <TextField
                        type="url"
                        style={textField}
                        error={!isValidUrl(url1)[1]}
                        onChange={(event, value) => {
                        setURL(0, event.target.value);
                    }}
                        onFocus={(event, value) => {
                        setURL(0, event.target.value);
                    }}
                        onBlur={(event, value) => {
                        setURL(0, event.target.value);
                    }}
                        id="url-field-1"
                        label=""
                        value={url1}
                        helperText={isValidUrl(url1)[1]
                        ? "Link 1"
                        : isValidUrl(url1)[0]}
                        placeholder="Ex. Wikipedia page."/>
                    <Fab
                        style={!t2
                        ? {
                            visibility: "visible"
                        }
                        : {
                            display: "none"
                        }}
                        onClick={() => {
                        setT2(true)
                    }}
                        className="setup-add-button"
                        color="secondary"
                        aria-label="add">
                        <AddIcon
                            style={{
                            color: "white"
                        }}/>
                    </Fab>
                    <TextField
                        type="url"
                        style={t2
                        ? textField
                        : {
                            visibility: "hidden"
                        }}
                        value={url2}
                        error={!isValidUrl(url2)[1]}
                        onChange={(event, value) => {
                        setURL(1, event.target.value);
                    }}
                        onFocus={(event, value) => {
                        setURL(1, event.target.value);
                    }}
                        onBlur={(event, value) => {
                        setURL(1, event.target.value);
                    }}
                        id="url-field-2"
                        label=""
                        helperText={isValidUrl(url2)[1]
                        ? "Link 2"
                        : isValidUrl(url2)[0]}
                        placeholder="Ex. News article."/>
                    <Fab
                        style={(!t3 && t2)
                        ? {
                            visibility: "visible"
                        }
                        : {
                            display: "none"
                        }}
                        onClick={() => {
                        setT3(true)
                    }}
                        className="setup-add-button"
                        color="secondary"
                        aria-label="add">
                        <AddIcon
                            style={{
                            color: "white"
                        }}/>
                    </Fab>
                    <TextField
                        type="url"
                        value={url3}
                        style={t3
                        ? textField
                        : {
                            visibility: "hidden"
                        }}
                        error={!isValidUrl(url3)[1]}
                        onFocus={(event, value) => {
                        setURL(2, event.target.value);
                    }}
                        onBlur={(event, value) => {
                        setURL(2, event.target.value);
                    }}
                        onChange={(event, value) => {
                        setURL(2, event.target.value);
                    }}
                        id="url-field-3"
                        value={url3}
                        helperText={isValidUrl(url3)[1]
                        ? "Link 3"
                        : isValidUrl(url3)[0]}
                        placeholder="Ex. Blog post."/>

                </div>
            </div>
            <div className="setup-bottom-section">

                <PageIndicator index={2} handler={moveToPage}/>

                <div className="setup-button">
                    <Button
                        variant="contained"
                        color="secondary"
                        size="small"
                        disabled={!props.isReady}
                        onClick={submitNewWorkshop}>
                        Generate
                    </Button>
                </div>
            </div>

        </div>
    );
}

export function Page3Desktop(props) {

    const [t2,
        setT2] = React.useState(props.urls[1]
        ? true
        : false)
    const [t3,
        setT3] = React.useState(props.urls[2]
        ? true
        : false)

    const [url1,
        setUrl1] = React.useState(props.urls[0]
        ? props.urls[0]
        : "");
    const [url2,
        setUrl2] = React.useState(props.urls[1]
        ? props.urls[1]
        : "");
    const [url3,
        setUrl3] = React.useState(props.urls[2]
        ? props.urls[2]
        : "");

    const setURL = (idx, value) => {
        if (idx === 0) {
            setUrl1(value);
        } else if (idx === 1) {
            setUrl2(value);
        } else if (idx === 2) {
            setUrl3(value);
        }

        props.setUrls([url1, url2, url3]);
    };

    const submitNewWorkshop = () => {
        setUrl1(document.getElementById("url-field-1").value);
        setUrl2(document.getElementById("url-field-2").value);
        setUrl3(document.getElementById("url-field-3").value);

        if (isValidUrl(url1)[1] || url1.length === 0) {
            props.setUrls([url1, url2, url3]);
            props.handleSubmit([url1, url2, url3]);
        } else {
            alert("There is something wrong with the info you have submitted")
        }
    };

    const moveToPage = val => {
        // console.log(val);
        props.handler(val);
    };
    return (
        <div className="flex-container page-1">

            <TopBar></TopBar>

            <div className="setup-row-stack">

                <div className="setup-row-div">

                    <div className="setup-anim-frame">
                        <Setup_Page3></Setup_Page3>
                        <div className=" page3-text">
                            Let’s build up my brain. Give me some context by pasting links to inspiring
                            articles or posts about this topic.
                        </div>

                    </div>

                </div>

                <div className="form">

                    <TextField
                    
                        style={textField}
                        error={!isValidUrl(url1)[1]}
                        onChange={(event, value) => {
                        setURL(0, event.target.value);
                    }}
                        onFocus={(event, value) => {
                        setURL(0, event.target.value);
                    }}
                        onBlur={(event, value) => {
                        setURL(0, event.target.value);
                    }}
                        id="url-field-1"
                        label=""
                        value={url1}
                        helperText={isValidUrl(url1)[1]
                        ? "Link 1"
                        : isValidUrl(url1)[0]}
                        placeholder="Ex. Wikipedia page."/>
                    <Fab
                        style={!t2
                        ? {
                            visibility: "visible"
                        }
                        : {
                            display: "none"
                        }}
                        onClick={() => {
                        setT2(true)
                    }}
                        className="setup-add-button"
                        color="secondary"
                        aria-label="add">
                        <AddIcon
                            style={{
                            color: "white"
                        }}/>
                    </Fab>
                    <TextField
                    
                        style={t2
                        ? textField
                        : {
                            visibility: "hidden"
                        }}
                        value={url2}
                        error={!isValidUrl(url2)[1]}
                        onChange={(event, value) => {
                        setURL(1, event.target.value);
                    }}
                        onFocus={(event, value) => {
                        setURL(1, event.target.value);
                    }}
                        onBlur={(event, value) => {
                        setURL(1, event.target.value);
                    }}
                        id="url-field-2"
                        label=""
                        helperText={isValidUrl(url2)[1]
                        ? "Link 2"
                        : isValidUrl(url2)[0]}
                        placeholder="Ex. News article."/>
                    <Fab
                        style={(!t3 && t2)
                        ? {
                            visibility: "visible"
                        }
                        : {
                            display: "none"
                        }}
                        onClick={() => {
                        setT3(true)
                    }}
                        className="setup-add-button"
                        color="secondary"
                        aria-label="add">
                        <AddIcon
                            style={{
                            color: "white"
                        }}/>
                    </Fab>
                    <TextField
                    
                        value={url3}
                        style={t3
                        ? textField
                        : {
                            visibility: "hidden"
                        }}
                        error={!isValidUrl(url3)[1]}
                        onFocus={(event, value) => {
                        setURL(2, event.target.value);
                    }}
                        onBlur={(event, value) => {
                        setURL(2, event.target.value);
                    }}
                        onChange={(event, value) => {
                        setURL(2, event.target.value);
                    }}
                        id="url-field-3"
                        value={url3}
                        helperText={isValidUrl(url3)[1]
                        ? "Link 3"
                        : isValidUrl(url3)[0]}
                        placeholder="Ex. Blog post."/>

                </div>

            </div>
            <div className="setup-bottom-section">

                <PageIndicator index={2} handler={moveToPage}/>

                <div className="setup-button">
                    <Button
                        variant="contained"
                        color="secondary"
                        size="small"
                        disabled={!props.isReady}
                        onClick={submitNewWorkshop}>
                        Generate
                    </Button>
                </div>
            </div>
        </div>
    );
}

export function Page4(props) {
    //   const classes = useStyles();
    const list = {
        visible: {
            opacity: 1,
            transition: {
                when: "beforeChildren",
                staggerChildren: 0.1
            }
        },
        hidden: {
            opacity: 0,
            transition: {
                when: "afterChildren"
            }
        }
    }

    const item = {
        visible: {
            opacity: 1,
            x: 0
        },
        hidden: {
            opacity: 0,
            x: -100
        }
    }
    return (
        <motion.div
            className="setup-page4"
            initial="hidden"
            animate="visible"
            variants={list}>
            <TopBar></TopBar>
            <div className="tutorial-content">
                <motion.div className="setup-item-stack" variants={item}>
                    <Media>
                    {({ breakpoints, currentBreakpoint }) => (
                <LottieAnim animation={"swipe"} width={50 * 2} height={window.innerHeight/6}></LottieAnim>
                    )}
                </Media>
                    <div className="setup-text">
                        <span className="text-large">
                            Swipe
                        </span><br/>
                        <span className="small-text">
                            through cards.
                        </span>
                    </div>
                </motion.div>

                <motion.div className="setup-item-stack" variants={item}>
                <LottieAnim animation={"tap"} width={50 * 2} height={window.innerHeight/6}></LottieAnim>

                    <div className="setup-text">
                        <span className="text-large">
                        {window.innerWidth>1024?" Click":"Tap"}
                        </span><br/>
                        <span className="small-text">
                            to switch words
                        </span>
                    </div>
                </motion.div>

                <motion.div className="setup-item-stack" variants={item}>
                <LottieAnim animation={"tap-hold"} width={50 * 2} height={window.innerHeight/6}></LottieAnim>

                    <div className="setup-text">
                        <span className="text-large">
                            {window.innerWidth>1024?" Click & Hold":"Tap & Hold"}
                        </span><br/>
                        <span className="small-text">
                            to edit words
                        </span>
                    </div>
                </motion.div>


                <motion.div className="setup-item-stack" variants={item}>
                <LottieAnim animation={"save"} width={50 * 2} height={window.innerHeight/6}></LottieAnim>

                    <div className="setup-text">
                        <span className="text-large">
                            Save cards
                        </span><br/>
                        <span className="small-text">
                            to your insights deck
                        </span>
                    </div>
                </motion.div>
            </div>

            <div className="loader">
                <div className="sweet-loading">
                    <button className="blue-button">
                        <ScaleLoader
                            css={override}
                            size={"20px"}
                            height={"15px"}
                            color={"#fff"}
                            loading={true}/>
                    </button>
                    <div className="setup-generating">
                        Generating...
                    </div>

                </div>
            </div>
        </motion.div>

    )
}