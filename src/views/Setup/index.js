import * as React from "react";

import {Page1, Page2, Page3, Page4, Page3Desktop} from './Pages'
import API from "../../services/API";
import {loadWorkshopFromID, loadDefault} from '../../store/updaters/loadWorkshops'
import {Media} from 'react-breakpoints'
import {Link} from 'react-router-dom'

export function Setup(props) {
    const [stage, setStage] = React.useState(1);
    const [isReady, setReady] = React.useState(false)
    const [error, setError] = React.useState("")
  
    // User details
    const [title, setTitle] = React.useState("");
    const [problem, setProblem] = React.useState("");
    const [urls, setUrls] = React.useState([]);
  
    function changeStage(value) {
      console.log(value);
      setStage(value);
    }

    function checkState() {
      console.log(isReady)
      if(title.length>0 && problem.length>0 && urls.length>0){
        setReady(true)
      } else {
        setReady(false)
      }
    }
  
    async function postToFirebase(_urls) {
      const workshop = {
        title: title,
        problem: problem,
        url1: _urls[0],
        url2: _urls[1]?_urls[1]:"",
        url3: _urls[2]?_urls[2]:"",
      };
      
      console.log(workshop)
      setStage(4)
  
      try {
        const response = await API.post('/setup', JSON.stringify(workshop));
        console.log('👉 Returned data:', response);

        //Should set selected workshop here
        if(response.status === 200) {
          let result = loadWorkshopFromID(response.data.id)
          if(result) {
              localStorage.setItem("session", JSON.stringify({time: new Date(), id: response.data.id }))
            props.history.push('/whatif')
          }
        }


      } catch (e) {
        console.log(`😱 Axios request failed: ${e}`);
        setStage(5)
        setError(e)
      }
    }
  
    if (stage === 1) {
      return (
        <Page1
          handler={(page) => {
            setStage(page);
            checkState()

          }}

          title={title}
          setTitle={title => {
            setTitle(title);
            checkState()
          }}
        />
      );
    } else if (stage === 2) {
      return (
        <Page2
          problem={problem}
          handler={(page) => {
            checkState()

            setStage(page);
          }}
          setProblem={problem => {
            setProblem(problem);
            checkState()

            console.log(problem, "upperlevel");
          }}
        />
      );
    } else if (stage === 3) {
      return (

        <Media>

          {({ breakpoints, currentBreakpoint }) =>
            breakpoints[currentBreakpoint] > breakpoints.mobileLandscape ? (
              <Page3Desktop

              urls={urls}

              isReady={isReady}
              handler={(page) => {
                setStage(page);
                checkState()

              }}
              setUrls={urls => {
                setUrls(urls);
                checkState()

                console.log(urls, "upper");
              }}
              handleSubmit={urls => {
                postToFirebase(urls);
              }}
            />
            ) : (
              <Page3
              urls={urls}

              isReady={isReady}
              handler={(page) => {
                setStage(page);
                checkState()

              }}
              setUrls={urls => {
                setUrls(urls);
                checkState()

                console.log(urls, "upper");
              }}
              handleSubmit={urls => {
                postToFirebase(urls);
              }}
            />
            )
          }         
        </Media>

      );
    } else if (stage === 4) {
      
      return (
        <Page4
          handler={(page) => {
            // setStage(page);
          }}
        />
      );
    } else if(stage===5) {
      return(
      <div className="page-1">
        <div className="text-large">
          Ooops there was a problem with your setup.
        </div>
        <Link to="/">Go Home</Link>

        {/* {error} */}
      </div>
      )
    } else {
      return <h1 />;
    }
  }
  