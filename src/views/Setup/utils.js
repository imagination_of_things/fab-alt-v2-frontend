export const isValidUrl = string => {
    if (string.length < 1) {
      return ["", true];
    }
    var n = string.search(".pdf");
  
    if (n >= 0) {
      return ["Cannot accept pdf files", false];
    }
  
    try {
      new URL(string);
      return ["", true];
    } catch (_) {
      return ["Not a valid url", false];
    }
  };
  